
plugin.tx_teufelsextdlc_teufelsextdlcapirequest {
	view {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcapirequest/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:teufels_ext_dlc/Resources/Private/Templates/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcapirequest/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:teufels_ext_dlc/Resources/Private/Partials/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcapirequest/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:teufels_ext_dlc/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcapirequest//a; type=string; label=Default storage PID
		storagePid =
	}
}

plugin.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage {
	view {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:teufels_ext_dlc/Resources/Private/Templates/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:teufels_ext_dlc/Resources/Private/Partials/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:teufels_ext_dlc/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage//a; type=string; label=Default storage PID
		storagePid =
	}
}

plugin.tx_teufelsextdlc_teufelsextdlcrenderlist {
	view {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlist/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:teufels_ext_dlc/Resources/Private/Templates/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlist/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:teufels_ext_dlc/Resources/Private/Partials/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlist/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:teufels_ext_dlc/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlist//a; type=string; label=Default storage PID
		storagePid =
	}
}

plugin.tx_teufelsextdlc_teufelsextdlcrenderfilter {
	view {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderfilter/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:teufels_ext_dlc/Resources/Private/Templates/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderfilter/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:teufels_ext_dlc/Resources/Private/Partials/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderfilter/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:teufels_ext_dlc/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderfilter//a; type=string; label=Default storage PID
		storagePid =
	}
}

plugin.tx_teufelsextdlc_teufelsextdlcrenderlistcategories {
	view {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistcategories/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:teufels_ext_dlc/Resources/Private/Templates/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistcategories/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:teufels_ext_dlc/Resources/Private/Partials/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistcategories/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:teufels_ext_dlc/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistcategories//a; type=string; label=Default storage PID
		storagePid =
	}
}

plugin.tx_teufelsextdlc_teufelsextdlcrenderlistlanguages {
	view {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistlanguages/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:teufels_ext_dlc/Resources/Private/Templates/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistlanguages/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:teufels_ext_dlc/Resources/Private/Partials/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistlanguages/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:teufels_ext_dlc/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistlanguages//a; type=string; label=Default storage PID
		storagePid =
	}
}

plugin.tx_teufelsextdlc_teufelsextdlcrenderlistpages {
	view {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistpages/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:teufels_ext_dlc/Resources/Private/Templates/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistpages/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:teufels_ext_dlc/Resources/Private/Partials/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistpages/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:teufels_ext_dlc/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderlistpages//a; type=string; label=Default storage PID
		storagePid =
	}
}

plugin.tx_teufelsextdlc_teufelsextdlcrenderjspages {
	view {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderjspages/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:teufels_ext_dlc/Resources/Private/Templates/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderjspages/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:teufels_ext_dlc/Resources/Private/Partials/
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderjspages/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:teufels_ext_dlc/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_teufelsextdlc_teufelsextdlcrenderjspages//a; type=string; label=Default storage PID
		storagePid =
	}
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

########################################
## CONSTANTS for  EXT:teufels_ext_dlc ##
########################################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:teufels_ext_dlc/Configuration/TypoScript/Constants/Production" extensions="txt">
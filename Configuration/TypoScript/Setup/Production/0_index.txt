##
## tx_extbase mapping
##
#config.tx_extbase {
#	persistence {
#		classes {
#			TEUFELS\TeufelsExtDlc\Domain\Model\Reference {
#				mapping {
#					tableName = sys_file
#					columns {
#						uid_local.mapOnProperty = originalFileIdentifier
#					}
#				}
#			}
#		}
#		objects {
#			TYPO3\CMS\Extbase\Domain\Model\File.className = TEUFELS\TeufelsExtDlc\Domain\Model\Reference
#		}
#		updateReferenceIndex = 1
#	}
#}

plugin.tx_teufelsextdlc {
    settings {
        production {
            includePath {
                public = {$plugin.tx_teufelsextdlc.settings.production.includePath.public}
                private = {$plugin.tx_teufelsextdlc.settings.production.includePath.private}
                frontend {
                    public = {$plugin.tx_teufelsextdlc.settings.production.includePath.frontend.public}
                }
            }
            api {
                typeNum = {$plugin.tx_teufelsextdlc.settings.production.api.typeNum}
                token = {$plugin.tx_teufelsextdlc.settings.production.api.token}
                version = {$plugin.tx_teufelsextdlc.settings.production.api.version}
                query {
                    country = {$plugin.tx_teufelsextdlc.settings.production.api.query.country}
                    language = {$plugin.tx_teufelsextdlc.settings.production.api.query.language}
                    filterKey = {$plugin.tx_teufelsextdlc.settings.production.api.query.filterKey}
                    ##
                    ## Delimiter
                    ##
                    delimiter {
                        ##
                        ## Main category for downloads
                        ##
                        sys_category = {$plugin.tx_teufelsextdlc.settings.production.api.query.delimiter.sys_category}
                        ##
                        ## File Storage for downloads
                        ## Don't use fileadmin
                        ##
                        storage {
                            uid = {$plugin.tx_teufelsextdlc.settings.production.api.query.delimiter.storage.uid}
                            basePath = {$plugin.tx_teufelsextdlc.settings.production.api.query.delimiter.storage.basePath}
                        }
                    }
                    ##
                    ## options for files
                    ##
                    files {
                        maxItems = {$plugin.tx_teufelsextdlc.settings.production.api.query.files.maxItems}
                        itemsPerPage = {$plugin.tx_teufelsextdlc.settings.production.api.query.files.itemsPerPage}
                        pagination {
                            showFirst = {$plugin.tx_teufelsextdlc.settings.production.api.query.files.pagination.showFirst}
                            showPrevious = {$plugin.tx_teufelsextdlc.settings.production.api.query.files.pagination.showPrevious}
                            showNext = {$plugin.tx_teufelsextdlc.settings.production.api.query.files.pagination.showNext}
                            showLast = {$plugin.tx_teufelsextdlc.settings.production.api.query.files.pagination.showLast}
                            insertBelow = {$plugin.tx_teufelsextdlc.settings.production.api.query.files.pagination.insertBelow}
                            insertAbove = {$plugin.tx_teufelsextdlc.settings.production.api.query.files.pagination.insertAbove}
                        }
                    }
                    ##
                    ## options for pages
                    ##
                    pages {
                        ##
                        ## comma separated value
                        ##
                        doktypes = {$plugin.tx_teufelsextdlc.settings.production.api.query.pages.doktypes}
                        ##
                        ## comma separated value
                        ## AND pages.nav_hide IN (0,1)
                        ## default 0
                        ##
                        andNavHideIn = {$plugin.tx_teufelsextdlc.settings.production.api.query.pages.andNavHideIn}

                        pidList = {$plugin.tx_teufelsextdlc.settings.production.api.query.pages.pidList}
                        levels = {$plugin.tx_teufelsextdlc.settings.production.api.query.pages.levels}
                    }

                    ##
                    ## options for filter
                    ##
                    filter {
                        pages {
                            show = {$plugin.tx_teufelsextdlc.settings.production.api.query.filter.pages.show}
                            input = {$plugin.tx_teufelsextdlc.settings.production.api.query.filter.pages.input}
                            type = {$plugin.tx_teufelsextdlc.settings.production.api.query.filter.pages.type}
                            levels = {$plugin.tx_teufelsextdlc.settings.production.api.query.pages.levels}
                        }
                        categories {
                            show = {$plugin.tx_teufelsextdlc.settings.production.api.query.filter.categories.show}
                            input = {$plugin.tx_teufelsextdlc.settings.production.api.query.filter.categories.input}
                        }
                        languages{
                            show = {$plugin.tx_teufelsextdlc.settings.production.api.query.filter.languages.show}
                            input = {$plugin.tx_teufelsextdlc.settings.production.api.query.filter.languages.input}
                        }
                    }
                }
            }
            action {
                detail = {$plugin.tx_teufelsextdlc.settings.production.action.detail}
                list = {$plugin.tx_teufelsextdlc.settings.production.action.list}
                request = {$plugin.tx_teufelsextdlc.settings.production.action.request}
            }
            cache {
                key = {$plugin.tx_teufelsextdlc.settings.production.cache.key}
                period = {$plugin.tx_teufelsextdlc.settings.production.cache.period}
            }
        }
    }
}

##
## Page Template with Backend Layout
##
page {
    10 {
        templateRootPaths {
            250 = EXT:teufels_ext_dlc/Resources/Private/Templates/Page
        }
    }
}

page {
    includeJSFooterlibs {
        tx_teufelsextdlc_render_filter = {$plugin.tx_teufelsextdlc.settings.production.includePath.public}Assets/Render/Filter/Js/index.js
        tx_teufelsextdlc_render_filter.async = 1
        tx_teufelsextdlc_render_listpages = {$plugin.tx_teufelsextdlc.settings.production.includePath.public}Assets/Render/ListPages/Js/index.js
        tx_teufelsextdlc_render_listpages.async = 1
        tx_teufelsextdlc_render_listcategories = {$plugin.tx_teufelsextdlc.settings.production.includePath.public}Assets/Render/ListCategories/Js/index.js
        tx_teufelsextdlc_render_listcategories.async = 1
    }
}

##
## Lib
##

##
## API Request
##
lib.tx_teufelsextdlc_teufelsextdlcapirequest = COA
lib.tx_teufelsextdlc_teufelsextdlcapirequest {
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = TeufelsExtDlc
		pluginName = Teufelsextdlcapirequest
		vendorName = TEUFELS
		controller = Api
		action = request
		settings =< plugin.tx_teufelsextdlc_teufelsextdlcapirequest.settings
		persistence =< plugin.tx_teufelsextdlc_teufelsextdlcapirequest.persistence
		view =< plugin.tx_teufelsextdlc_teufelsextdlcapirequest.view
	}
}

##
## List Files on Current Page
##
plugin.tx_teufelsextdlc_teufelsextdlcrenderlistpages {
    settings {
        production {
            api {
                query {
                    pages {
                        pidList = {$plugin.tx_teufelsextdlc.settings.production.api.query.pages.pidList}
                    }
                }
            }
        }
    }
}
lib.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage = COA
lib.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage {
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = TeufelsExtDlc
		pluginName = Teufelsextdlcrenderlistoncurrentpage
		vendorName = TEUFELS
		controller = Render
		action = listOnCurrentPage
		settings =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage.settings
		persistence =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage.persistence
		view =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage.view
	}
}

##
## jsPages
##
plugin.tx_teufelsextdlc_teufelsextdlcrenderjspages {
    settings {
        production {
            api {
                query {
                    pages {
                        pidList = {$plugin.tx_teufelsextdlc.settings.production.api.query.pages.pidList}
                        levels = {$plugin.tx_teufelsextdlc.settings.production.api.query.pages.levels}
                    }
                }
            }
        }
    }
}
lib.tx_teufelsextdlc_teufelsextdlcrenderjspages = COA
lib.tx_teufelsextdlc_teufelsextdlcrenderjspages {
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = TeufelsExtDlc
		pluginName = Teufelsextdlcrenderjspages
		vendorName = TEUFELS
		controller = Render
		action = jsPages
		settings =< plugin.tx_teufelsextdlc_teufelsextdlcrenderjspages.settings
		persistence =< plugin.tx_teufelsextdlc_teufelsextdlcrenderjspages.persistence
		view =< plugin.tx_teufelsextdlc_teufelsextdlcrenderjspages.view
	}
}

##
## HIDDEN INPUT FIELDS
## <input type="hidden"...
##
lib.tx_teufelsextdlc_teufelsextdlcrenderlist__HTML__input_hidden_pages = TEXT
lib.tx_teufelsextdlc_teufelsextdlcrenderlist__HTML__input_hidden_pages.value (
<input type="hidden" name="tx_teufelsextdlc_teufelsextdlcrenderlist[sys_file_metadata_pages_mm][mm]" id="sys_file_metadata_pages_mm"/>
)
lib.tx_teufelsextdlc_teufelsextdlcrenderfilter__HTML__input_hidden_pages = TEXT
lib.tx_teufelsextdlc_teufelsextdlcrenderfilter__HTML__input_hidden_pages.value (
<input type="hidden" name="tx_teufelsextdlc_teufelsextdlcrenderfilter[sys_file_metadata_pages_mm][mm]" />
)

##
## Categories
## <input type="hidden"...
##
lib.tx_teufelsextdlc_teufelsextdlcrenderlist__HTML__input_hidden_categories = TEXT
lib.tx_teufelsextdlc_teufelsextdlcrenderlist__HTML__input_hidden_categories.value (
<input type="hidden" name="tx_teufelsextdlc_teufelsextdlcrenderlist[sys_category_record_mm][mm]" class="hidden sys_category_record_mm" />
)
lib.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage__HTML__input_hidden_categories = TEXT
lib.tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage__HTML__input_hidden_categories.value (
<input type="hidden" name="tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage[sys_category_record_mm][mm]" class="hidden sys_category_record_mm" />
)
lib.tx_teufelsextdlc_teufelsextdlcrenderlistcategories__HTML__input_hidden_categories = TEXT
lib.tx_teufelsextdlc_teufelsextdlcrenderlistcategories__HTML__input_hidden_categories.value (
<input type="hidden" name="tx_teufelsextdlc_teufelsextdlcrenderlistcategories[sys_category_record_mm][mm]" class="hidden sys_category_record_mm" />
)
lib.tx_teufelsextdlc_teufelsextdlcrenderfilter__HTML__input_hidden_categories = TEXT
lib.tx_teufelsextdlc_teufelsextdlcrenderfilter__HTML__input_hidden_categories.value (
<input type="hidden" name="tx_teufelsextdlc_teufelsextdlcrenderfilter[sys_category_record_mm][mm]" class="hidden sys_category_record_mm appendToPagination" />
)


lib.tx_teufelsextdlc_teufelsextdlcrenderlist__HTML__input_hidden_languages = TEXT
lib.tx_teufelsextdlc_teufelsextdlcrenderlist__HTML__input_hidden_languages.value (
<input type="hidden" name="tx_teufelsextdlc_teufelsextdlcrenderlist[sys_file_metadata_sys_language_mm][mm]" id="sys_file_metadata_sys_language_mm"/>
)
lib.tx_teufelsextdlc_teufelsextdlcrenderfilter__HTML__input_hidden_languages = TEXT
lib.tx_teufelsextdlc_teufelsextdlcrenderfilter__HTML__input_hidden_languages.value (
<input type="hidden" name="tx_teufelsextdlc_teufelsextdlcrenderfilter[sys_file_metadata_sys_category_mm][mm]" />
)

##
## Render :: Category List
##
lib.tx_teufelsextdlc_teufelsextdlcrenderlistcategories = COA
lib.tx_teufelsextdlc_teufelsextdlcrenderlistcategories {
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = TeufelsExtDlc
		pluginName = Teufelsextdlcrenderlistcategories
		vendorName = TEUFELS
		controller = Render
		action = listCategories
		settings =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistcategories.settings
		persistence =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistcategories.persistence
		view =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistcategories.view
	}
}
plugin.tx_teufelsextdlc_teufelsextdlcrenderlistcategories.view.pluginNamespace = tx_teufelsextdlc_teufelsextdlcrenderlist

##
## Render :: Language List
##
lib.tx_teufelsextdlc_teufelsextdlcrenderlistlanguages = COA
lib.tx_teufelsextdlc_teufelsextdlcrenderlistlanguages {
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = TeufelsExtDlc
		pluginName = Teufelsextdlcrenderlistlanguages
		vendorName = TEUFELS
		controller = Render
		action = listLanguages
		settings =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistlanguages.settings
		persistence =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistlanguages.persistence
		view =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistlanguages.view
	}
}

##
## Render :: Page List
##
lib.tx_teufelsextdlc_teufelsextdlcrenderlistpages = COA
lib.tx_teufelsextdlc_teufelsextdlcrenderlistpages {
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = TeufelsExtDlc
		pluginName = Teufelsextdlcrenderlistpages
		vendorName = TEUFELS
		controller = Render
		action = listPages
		settings =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistpages.settings
		persistence =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistpages.persistence
		view =< plugin.tx_teufelsextdlc_teufelsextdlcrenderlistpages.view
	}
}
plugin.tx_teufelsextdlc_teufelsextdlcrenderlistpages.view.pluginNamespace = tx_teufelsextdlc_teufelsextdlcrenderlist


##
## PAGE for API
##

[PIDinRootline = {$plugin.tx_teufelsextdlc.settings.production.action.request}] && [globalVar = GP:token={$plugin.tx_teufelsextdlc.settings.production.api.token}]

api_tx_teufelsextdlc_teufelsextdlcapirequest = PAGE
api_tx_teufelsextdlc_teufelsextdlcapirequest {

    typeNum = {$plugin.tx_teufelsextdlc.settings.production.api.typeNum}
    10 < lib.tx_teufelsextdlc_teufelsextdlcapirequest

    config {
        disableAllHeaderCode = 1
        additionalHeaders = Content-type:application/json; charset=utf-8
        xhtml_cleaning = 0
        admPanel = 0
        debug = 0
        no_cache = 1
    }

}

[global]
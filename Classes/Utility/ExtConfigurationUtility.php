<?php

namespace TEUFELS\TeufelsExtDlc\Utility;

//use TEUFELS\TeufelsExtDlc\Domain\Model\Dto\ExtConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/**
 * Typoscript configuration for authors extension
 */
class ExtConfigurationUtility
{
    /**
     * Get authors extension typoscript settings and transfer to object (so we can work comfortable with getter)
     *
     * @return object
     */
    public static function getSettings()
    {



        //$setting = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);

        // make instance ObjectManager
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

        //$aSettingsDlc = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);

        // get ConfigurationManagerInterface
        //$configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');

        // get typoscript configurations from all installed extensions
        $fullTyposcriptSettings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        // extract authors' typoscript settings
        $aTxExtDlcTyposcriptSettings = $fullTyposcriptSettings['plugin.']['tx_teufelsextdlc.']['settings.'];

        // include created DTO (data transfer object) model class (with getter for typoscript settings)
        //GeneralUtility::requireOnce(ExtensionManagementUtility::extPath('teufels_ext_dlc') . 'Classes/Domain/Model/Dto/ExtConfiguration.php');

        // create object with extracted settings from above
        //$tsSettings = new ExtConfiguration($txExtDlcTyposcriptSettings);


        // return object with typoscript settings
        //return $tsSettings;
        return $aTxExtDlcTyposcriptSettings;
    }

}
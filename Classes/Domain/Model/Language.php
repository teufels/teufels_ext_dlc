<?php
namespace TEUFELS\TeufelsExtDlc\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Language
 */
class Language extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * ISO 639 is a standardized nomenclature used to classify languages. Each language
     * is assigned a two-letter (639-1) and three-letter (639-2 and 639-3), lowercase
     * abbreviation, amended in later versions of the nomenclature.
     *
     * @var string
     */
    protected $iso6391 = '';
    
    /**
     * ISO 3166-1 is part of the ISO 3166 standard published by the International
     * Organization for Standardization (ISO), and defines codes for the names of
     * countries, dependent territories, and special areas of geographical interest.
     * The official name of the standard is Codes for the representation of names of
     * countries and their subdivisions – Part 1: Country codes.
     *
     * @var string
     */
    protected $iso31661 = '';
    
    /**
     * Returns the iso6391
     *
     * @return string $iso6391
     */
    public function getIso6391()
    {
        return $this->iso6391;
    }
    
    /**
     * Sets the iso6391
     *
     * @param string $iso6391
     * @return void
     */
    public function setIso6391($iso6391)
    {
        $this->iso6391 = $iso6391;
    }
    
    /**
     * Returns the iso31661
     *
     * @return string $iso31661
     */
    public function getIso31661()
    {
        return $this->iso31661;
    }
    
    /**
     * Sets the iso31661
     *
     * @param string $iso31661
     * @return void
     */
    public function setIso31661($iso31661)
    {
        $this->iso31661 = $iso31661;
    }

}
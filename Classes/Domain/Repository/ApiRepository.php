<?php
namespace TEUFELS\TeufelsExtDlc\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Apis
 */
class ApiRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    const bDEBUG = 0;
    
    const PLACE_AT_END_OF_LIST = 'Zzzz';
    const SORTING_PLACE_AT_END_OF_LIST = 999999999999;
    
    const aTABLE = array('sys_file', 'sys_file_metadata');
    
    const aTABLELEFTJOIN = array('sys_category', 'tx_teufelsextdlc_domain_model_language');
    
    const aFROM = array(self::aTABLE[0], self::aTABLE[1] . ' LEFT JOIN ' . self::aTABLELEFTJOIN[0] . ' ON  ' . self::aTABLE[1] . '.' . self::aTABLELEFTJOIN[0] . ' = ' . self::aTABLELEFTJOIN[0] . '.uid ' . ' LEFT JOIN ' . self::aTABLELEFTJOIN[1] . ' ON  ' . self::aTABLE[1] . '.' . self::aTABLELEFTJOIN[1] . ' = ' . self::aTABLELEFTJOIN[1] . '.uid ');
    
    const aSELECT = array(self::aTABLE[0] . '.uid AS ' . self::aTABLE[0] . '__uid', 'SUBSTRING_INDEX(SUBSTRING_INDEX(' . self::aTABLE[0] . '.identifier,\'/\',-2),\'/\',1) AS ' . self::aTABLE[0] . '__folder',  self::aTABLE[0] . '.name AS ' . self::aTABLE[0] . '__name', self::aTABLE[0] . '.extension AS ' . self::aTABLE[0] . '__extension', 'CONCAT(\'%%%basePath%%%\',' . self::aTABLE[0] . '.identifier) AS ' . self::aTABLE[0] . '__identifier', self::aTABLE[0] . '.size AS ' . self::aTABLE[0] . '__size', self::aTABLE[0] . '.mime_type AS ' . self::aTABLE[0] . '__mime_type', self::aTABLE[1] . '.sortkey AS ' . self::aTABLE[1] . '__sortkey', self::aTABLE[1] . '.link_to AS ' . self::aTABLE[1] . '__link_to', self::aTABLELEFTJOIN[0] . '.uid AS ' . self::aTABLELEFTJOIN[0] . '__uid', 'IFNULL(' . self::aTABLELEFTJOIN[0] . '.sorting, \'' . self::SORTING_PLACE_AT_END_OF_LIST . '\') AS ' . self::aTABLELEFTJOIN[0] . '__sorting', 'IFNULL(' . self::aTABLELEFTJOIN[0] . '.title, \'' . self::PLACE_AT_END_OF_LIST . '\') AS ' . self::aTABLELEFTJOIN[0] . '__title', 'IFNULL(' . self::aTABLELEFTJOIN[1] . '.iso6391, \'\') AS ' . self::aTABLELEFTJOIN[1] . '__iso6391', 'IFNULL(' . self::aTABLELEFTJOIN[1] . '.iso31661, \'\') AS ' . self::aTABLELEFTJOIN[1] . '__iso31661');
    
    const aSELECT_LOCALIZE = array('IFNULL(' . self::aTABLE[1] . '.title, \'\') AS ' . self::aTABLE[1] . '__title', 'IFNULL(' . self::aTABLE[1] . '.description, \'\') AS ' . self::aTABLE[1] . '__description', 'IFNULL(' . self::aTABLE[1] . '.keywords, \'\') AS ' . self::aTABLE[1] . '__keywords', self::aTABLE[1] . '.sys_language_uid AS ' . self::aTABLE[1] . '__sys_language_uid');
    
    const aSELECTSIMPLE = array(self::aTABLE[0] . '.uid AS ' . self::aTABLE[0] . '__uid', self::aTABLE[1] . '.sortkey AS ' . self::aTABLE[1] . '__sortkey', 'IFNULL(' . self::aTABLELEFTJOIN[0] . '.title, \'\') AS ' . self::aTABLELEFTJOIN[0] . '__title');
    
    const aORDERBY = array(self::aTABLELEFTJOIN[0] . '__sorting ASC', self::aTABLELEFTJOIN[0] . '__title ASC', self::aTABLE[0] . '__folder', self::aTABLE[1] . '__sortkey ASC', self::aTABLE[1] . '__title ASC');
    
    const aGROUPBY = array(self::aTABLE[0] . '__uid');
    
    /**
     * languageRepository
     *
     * @var \TEUFELS\TeufelsExtDlc\Domain\Repository\LanguageRepository
     * @inject
     */
    protected $languageRepository = NULL;
    
    /**
     * categoryRepository
     *
     * @var \TEUFELS\TeufelsExtDlc\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository = NULL;
    
    /**
     * pageRepository
     *
     * @var \TEUFELS\TeufelsExtDlc\Domain\Repository\PageRepository
     * @inject
     */
    protected $pageRepository = NULL;
    
    //TODO clean up code
    //TODO remove simple? maybe?
    /**
     * syscategoriesRepository
     *
     * @var \TYPO3\CMS\Extbase\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $syscategoriesRepository = null;
    
    /**
     * @param $bSimple
     * @param $iLimit
     * @param $iOffset
     * @param $aSettings
     * @param $aFilter
     */
    public function apiCallList($bSimple = FALSE, $iLimit = 999, $iOffset = 0, $aSettings = array(), $aFilter = array())
    {
        $iStorage = $aSettings['production']['api']['query']['delimiter']['storage']['uid'];
        $sStorage = $aSettings['production']['api']['query']['delimiter']['storage']['basePath'];
        $iSysCategory = $aSettings['production']['api']['query']['delimiter']['sys_category'];
        /*
         * Check if filter table exist
         */
        
        /*
         * current sys_language_uid
         */
        
        $iSysLanguageUid = intval($GLOBALS['TSFE']->sys_language_uid);
        $sSELECT = implode(',', self::aSELECTSIMPLE);
        if (!$bSimple) {
            $sSELECT = implode(',', self::aSELECT) . ', ' . implode(',', self::aSELECT_LOCALIZE);
        }
        /*
         * Replace Placeholder for base path
         */
        
        $sSELECT = str_replace('%%%basePath%%%', $sStorage, $sSELECT);
        //TODO Signal/Slot before extending FROM and WHERE
        /*
         * From
         */
        
        $sFROM = $this->extendFrom($aFilter);
        /*
         * Where
         */
        
        $sWHERE = self::aTABLE[0] . '.uid = ' . self::aTABLE[1] . '.file AND ' . self::aTABLE[0] . ".storage = {$iStorage} AND NOT(" . self::aTABLE[0] . '.missing) ';
        $sWHERE .= ' AND ' . self::aTABLE[1] . '.sys_language_uid IN (0,-1) ';
        /*
         * MM and M1
         */
        
        $sWHERE = $this->extendWhereByTable($sWHERE, $aFilter);
        /*
         * lt, lte, eq, neq, gte, gt
         */
        
        $sWHERE = $this->extendWhereByField($sWHERE, $aFilter);
        /*
         * Group by
         */
        
        $sGROUPBY = '';
        //implode(',', self::aGROUPBY);;
        /*
         * Order by
         */
        
        $sORDERBY = implode(',', self::aORDERBY);
        $sLIMITOFFSET = "{$iLimit} OFFSET {$iOffset}";
        if (self::bDEBUG) {
            var_dump('SELECT ' . $sSELECT . ' FROM ' . $sFROM . ' WHERE ' . $sWHERE . ' ORDER BY ' . $sORDERBY . ' LIMIT ' . $sLIMITOFFSET);
        }
        $aResult = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows($sSELECT, $sFROM, $sWHERE, $sGROUPBY, $sORDERBY, $sLIMITOFFSET, self::aTABLE[0] . '__uid');
        /*
         * Localize values inkl. sys_category.title
         */
        
//        if (self::bDEBUG) {
//            print '... ';
//            var_dump($aResult);
//            print ' ...';
//        }
        if ($iSysLanguageUid != 0 && !$bSimple) {
            foreach ($aResult as $aResult_) {
                $sSELECTL = self::aTABLE[0] . '.uid AS ' . self::aTABLE[0] . '__uid, ' . implode(',', self::aSELECT_LOCALIZE);
                $sFROML = self::aTABLE[0] . ', ' . self::aTABLE[1];
                $sWHEREL = self::aTABLE[0] . '.uid = ' . $aResult_['sys_file__uid'] . ' AND ' . self::aTABLE[0] . '.uid = ' . self::aTABLE[1] . '.file ' . 'AND ' . self::aTABLE[1] . ".sys_language_uid IN ({$iSysLanguageUid})";
                if (self::bDEBUG) {
                    var_dump('SELECT ' . $sSELECTL . ' FROM ' . $sFROML . ' WHERE ' . $sWHEREL);
                }
                $aResult1 = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows($sSELECTL, $sFROML, $sWHEREL, '', '', '', self::aTABLE[0] . '__uid');
                if (count($aResult1) == 1) {
                    $aResult = array_replace_recursive($aResult, $aResult1);
                }
            }
        }
        /*
         * Rearrange Result Array
         * => No String Keys
         * => Array instead of Object
         */
        
        $aResultFormated = array();
        foreach ($aResult as $aResult_) {
            $aResultFormated[] = $aResult_;
        }
        return $aResultFormated;
    }
    
    /**
     * @param $aFilter
     * @return string
     */
    private function extendFrom($aFilter)
    {
        $aFromTemp = self::aFROM;
        foreach ($aFilter as $sTable => $aValue) {
            if (is_array($aValue)) {
                foreach ($aValue as $sKey_ => $sValue_) {
                    switch ($sKey_) {
                        case 'mm':
                        case 'm1':    $aFromTemp[] = $sTable . ' AS ' . md5($sTable);
                            break;
                        default:
                    }
                }
            }
        }
        $sFROM = implode(',', $aFromTemp);
        return $sFROM;
    }
    
    /**
     * @param string $sRelationType
     * @param $sWHERE
     * @param $aFilter
     * @return string
     */
    private function extendWhereByTable($sWHERE, $aFilter)
    {
        /*
                 *  e.g.
                    $aFilter = array(1) {
                      ["sys_file_metadata_pages_mm"]=>
                      array(1) {
                        ["mm"]=>
                        string(4) "1,2;3,4"
                      }
                    }
           or
           $aFilter = array(1) {
                      ["tx_teufelsextdlc_domain_model_language"]=>
                      array(1) {
                        ["m1"]=>
                        string(4) "1,2"
                      }
                    }
        */
        
        foreach ($aFilter as $sTable => $aValue) {
            if (is_array($aValue)) {
                foreach ($aValue as $sKey_ => $sValue_) {
                    switch ($sKey_) {
                        case 'mm':
                        case 'm1':
                            $aAnd = explode(';', $sValue_);
                            $a = 0;
                            $sWHERE .= 'AND ( ';
                            foreach ($aAnd as $sOr) {
                                if ($a > 0) {
                                    $sWHERE .= ' AND ';
                                }
                                $aOr = explode(',', $sOr);
                                $o = 0;
                                $sWHERE .= '( ';
                                foreach ($aOr as $iOr_) {
                                    if ($o > 0) {
                                        $sWHERE .= ' OR ';
                                    }
                                    switch ($sKey_) {
                                        case 'mm':
                                            $sKeyAs = md5($sTable);
                                            if (is_numeric($iOr_)) {
                                                switch ($sTable) {
                                                    case 'sys_category_record_mm':    /*
                                                     * sys_category_record_mm
                                                     *
                                                     * +-----------+-------------+------------+ ...
                                                     * | uid_local | uid_foreign | tablenames | ...
                                                     * +-----------+-------------+------------+ ...
                                                     *
                                                     */

                                                        $sWHERE .= "({$sKeyAs}.uid_local = {$iOr_} " . "AND {$sKeyAs}.uid_foreign = " . self::aTABLE[1] . '.uid ' . "AND {$sKeyAs}.tablenames = '" . self::aTABLE[1] . '\') ';
                                                        break;
                                                    default:
                                                        $sWHERE .= "({$sKeyAs}.uid_foreign = {$iOr_} " . "AND {$sKeyAs}.uid_local = " . self::aTABLE[1] . '.uid) ';
                                                }
                                                $o++;
                                            }
                                            break;
                                        case 'm1':
                                            $sKeyAs = md5($sTable);
                                            if (is_numeric($iOr_)) {
                                                if (intval($iOr_) == 0) {
                                                    $sWHERE .= '(' . self::aTABLE[1] . ".{$sTable} = {$iOr_} ) ";
                                                } else {
                                                    $sWHERE .= '(' . self::aTABLE[1] . ".{$sTable} = {$iOr_} " . 'AND ' . self::aTABLE[1] . ".{$sTable} = {$sKeyAs}.uid) ";
                                                }
                                                $o++;
                                            }
                                            break;
                                        default:
                                    }
                                }
                                // / OR
                                $sWHERE .= ') ';
                                $a++;
                            }
                            // / AND
                            $sWHERE .= ') ';
                            break;
                        default:
                    }
                }
            }
        }
        return $sWHERE;
    }
    
    /**
     * @param $sWHERE
     * @param $aFilter
     * @return string
     */
    private function extendWhereByField($sWHERE, $aFilter = array())
    {
        /*
                 *  e.g.
                    $aFilter = array(1) {
                      ["move_to_archive_date"]=>
                      array(2) {
                        ["lt"]=>
                        string(4) "1234"
                        ["gt"]=>
                        string(2) "12"
                      }
                    }
        */
        
        foreach ($aFilter as $sField => $aValue) {
            if (is_array($aValue)) {
                foreach ($aValue as $sKey_ => $sValue_) {
                    switch ($sKey_) {
                        case 'lt':
                        case 'lte':
                        case 'eq':
                        case 'neq':
                        case 'gte':
                        case 'gt':
                            $aAnd = explode(';', $sValue_);
                            $a = 0;
                            $sWHERE .= 'AND ( ';
                            foreach ($aAnd as $sOr) {
                                if ($a > 0) {
                                    $sWHERE .= ' AND ';
                                }
                                $aOr = explode(',', $sOr);
                                $o = 0;
                                $sWHERE .= '( ';
                                foreach ($aOr as $sOr_) {
                                    if ($o > 0) {
                                        $sWHERE .= ' OR ';
                                    }
                                    switch ($sKey_) {
                                        case 'lt':
                                            if (is_numeric($sOr_)) {
                                                /*
                                                 * we assume that default is 0
                                                 */

                                                $sWHERE .= '(' . self::aTABLE[1] . '.' . $sField . ' < ' . $sOr_ . ' ';
                                                $sWHERE .= 'AND ' . self::aTABLE[1] . '.' . $sField . ' <> 0) ';
                                                $o++;
                                            }
                                            break;
                                        case 'lte':
                                            if (is_numeric($sOr_)) {
                                                $sWHERE .= self::aTABLE[1] . '.' . $sField . ' <= ' . $sOr_ . ' ';
                                                $o++;
                                            }
                                            break;
                                        case 'eq':
                                            if (is_numeric($sOr_)) {
                                                $sWHERE .= self::aTABLE[1] . '.' . $sField . ' = ' . $sOr_ . ' ';
                                                $o++;
                                            } else {
                                                $sWHERE .= self::aTABLE[1] . '.' . $sField . ' = \'' . $sOr_ . '\' ';
                                                $o++;
                                            }
                                            break;
                                        case 'neq':
                                            if (is_numeric($sOr_)) {
                                                $sWHERE .= self::aTABLE[1] . '.' . $sField . ' <> ' . $sOr_ . ' ';
                                                $o++;
                                            } else {
                                                $sWHERE .= self::aTABLE[1] . '.' . $sField . ' <> \'' . $sOr_ . '\' ';
                                                $o++;
                                            }
                                            break;
                                        case 'gte':
                                            if (is_numeric($sOr_)) {
                                                $sWHERE .= self::aTABLE[1] . '.' . $sField . ' >= ' . $sOr_ . ' ';
                                                $o++;
                                            }
                                            break;
                                        case 'gt':
                                            if (is_numeric($sOr_)) {
                                                $sWHERE .= self::aTABLE[1] . '.' . $sField . ' > ' . $sOr_ . ' ';
                                                $o++;
                                            }
                                            break;
                                        default:
                                    }
                                }
                                // / OR
                                $sWHERE .= ') ';
                                $a++;
                            }
                            // / AND
                            $sWHERE .= ') ';
                            break;
                        default:
                    }
                }
            }
        }
        return $sWHERE;
    }
    
    /**
     * @param $iLimit
     * @param $iOffset
     * @param $aSettings
     */
    public function apiCallCategories($iLimit = 999, $iOffset = 0, $aSettings = array())
    {
        $iSysCategory = intval($aSettings['production']['api']['query']['delimiter']['sys_category']);
        $aResult = $this->categoryRepository->findByParent($iSysCategory);
        //var_dump($aCategories);
        //        die;
        //        $iSysLanguageUid = intval($GLOBALS['TSFE']->sys_language_uid);
        //        $aTABLE_C = array(
        //            'sys_category'
        //        );
        //        $aSELECT_C = array(
        //            $aTABLE_C[0] . '.uid AS ' . $aTABLE_C[0] . '__uid',
        //            $aTABLE_C[0] . '.title AS ' . $aTABLE_C[0] . '__title',
        //            $aTABLE_C[0] . '.sys_language_uid AS ' . $aTABLE_C[0] . '__sys_language_uid'
        //        );
        //        $aSELECT_LOCALIZE_C = array(
        //            $aTABLE_C[0] . '.title AS ' . $aTABLE_C[0] . '__title'
        //        );
        //        $aFROM_C = array(
        //            $aTABLE_C[0]
        //        );
        //        $aWHERE_C = array(
        //            "{$aTABLE_C['0']}.sys_language_uid IN (0,-1)",
        //            "AND {$aTABLE_C['0']}.parent = {$iSysCategory}",
        //            "AND NOT({$aTABLE_C['0']}.deleted)",
        //            "AND NOT({$aTABLE_C['0']}.hidden)"
        //        );
        //        $aORDERBY_C = array(
        //            $aTABLE_C[0] . '__title ASC'
        //        );
        //        $aGROUPBY_C = array(
        //        );
        //         * SELECT ... FROM ... WHERE ... GROUP BY ... ORDER BY
        //        $sSELECT = ' ' . implode(', ', $aSELECT_C) . ' ';
        //        $sFROM = ' ' . implode(', ', $aFROM_C) . ' ';
        //        $sWHERE = ' ' . implode(' ', $aWHERE_C) . ' ';
        //        $sORDERBY = count($aORDERBY_C) > 0 ? ' ' . implode(', ', $aORDERBY_C) . ' ' : '';
        //        $sGROUPBY = count($aGROUPBY_C) > 0 ? ' ' . implode(', ', $aGROUPBY_C) . ' ' : '';
        //        $sLIMITOFFSET = "{$iLimit} OFFSET {$iOffset}";
        //        if (self::bDEBUG) {
        //            var_dump('SELECT ' . $sSELECT . ' FROM ' . $sFROM . ' WHERE ' . $sWHERE . ' ORDER BY ' . $sORDERBY);
        //        }
        //        $aResult = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows($sSELECT, $sFROM, $sWHERE, $sGROUPBY, $sORDERBY, $sLIMITOFFSET, $aTABLE_C[0] . '__uid');
        //        if ($iSysLanguageUid > 0) {
        //            foreach ($aResult as $aRow) {
        //                $aSELECT_C_L = array(
        //                    $aTABLE_C[0] . '.l10n_parent AS ' . $aTABLE_C[0] . '__uid',
        //                    $aTABLE_C[0] . '.title AS ' . $aTABLE_C[0] . '__title',
        //                    $aTABLE_C[0] . '.sys_language_uid AS ' . $aTABLE_C[0] . '__sys_language_uid'
        //                );
        //                $aWHERE_C_L = array(
        //                    "{$aTABLE_C['0']}.sys_language_uid IN ({$iSysLanguageUid})",
        //                    "AND {$aTABLE_C['0']}.l10n_parent = " . $aRow[$aTABLE_C[0] . '__uid'],
        //                    $aWHERE_C[1],
        //                    $aWHERE_C[2],
        //                    $aWHERE_C[3]
        //                );
        //                 * SELECT ... FROM ... WHERE ... GROUP BY ... ORDER BY
        //                $sSELECT_L = ' ' . implode(', ', $aSELECT_C_L) . ' ';
        //                $sFROM_L = ' ' . implode(', ', $aFROM_C) . ' ';
        //                $sWHERE_L = ' ' . implode(' ', $aWHERE_C_L) . ' ';
        //                $sORDERBY_L = '';
        //                $sGROUPBY_L = '';
        //                $sLIMITOFFSET_L = '';
        //                if (self::bDEBUG) {
        //                    var_dump('SELECT ' . $sSELECT_L . ' FROM ' . $sFROM_L . ' WHERE ' . $sWHERE_L . ' ORDER BY ' . $sORDERBY_L);
        //                }
        //                $aResult1 = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows($sSELECT_L, $sFROM_L, $sWHERE_L, $sGROUPBY_L, $sORDERBY_L, $sLIMITOFFSET_L, $aTABLE_C[0] . '__uid');
        //                if (count($aResult1) == 1) {
        //                    $aResult = array_replace_recursive($aResult, $aResult1);
        //                }
        //            }
        //        }
        /*
         * Rearrange Result Array
         * => No String Keys
         * => Array instead of Object
         */
        
        $aResultFormated = array();
        foreach ($aResult as $aRow) {
            $aResultFormated[] = $aRow;
        }
        return $aResultFormated;
    }
    
    /**
     * @param $iLimit
     * @param $iOffset
     * @param $aSettings
     */
    public function apiCallLanguages($iLimit = 999, $iOffset = 0, $aSettings = array())
    {
        $aResult = $this->languageRepository->findAll($iLimit, $iOffset, $aSettings);
        return $aResult;
    }
    
    /**
     * @param int $sPidList
     * @param array $aSettings
     * @return array
     */
    public function apiCallPagesByPid($sPidList = '', $aSettings = array())
    {
        $aResult = $this->pageRepository->findByPid($sPidList, $aSettings);
        return $aResult;
    }

    /**
     * @param int $sPidList
     * @param array $aSettings
     * @return array
     */
    public function apiCallPagesByPidListAndLevels($sPidList = '', $iLevels = 1, $aSettings = array())
    {
        $aResult = $this->pageRepository->findByPidListAndLevels($sPidList, $iLevels, $aSettings);
        return $aResult;
    }

}
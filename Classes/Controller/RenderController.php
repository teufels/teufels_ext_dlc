<?php
namespace TEUFELS\TeufelsExtDlc\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * RenderController
 */
class RenderController extends \TEUFELS\TeufelsExtDlc\Controller\AbstractController
{

    /**
     * renderRepository
     *
     * @var \TEUFELS\TeufelsExtDlc\Domain\Repository\RenderRepository
     * @inject
     */
    protected $renderRepository = NULL;

    /**
     * apiRepository
     *
     * @var \TEUFELS\TeufelsExtDlc\Domain\Repository\ApiRepository
     * @inject
     */
    protected $apiRepository = NULL;
    
    /**
     * action list
     *
     * @return mixed
     */
    public function listAction()
    {
        /*
         * aBody
         */
        
        $aBody = array();
        $aBody['bError'] = 0;
        $aBody['sDescription'] = array();
        $aBody['aApiResponse'] = array();
        $aBody['aDebug'] = array(
            'sys_language_uid' => $this->getSysLanguageUid(),
            'settings' => $this->getSettingsArray()
        );
        /*
         * Settings
         */
        
        $aSettings = $this->getSettingsArray();
        /*
         * get page id of request action
         */
        
        $iRequestPageUid = $this->getRequestPageUid();
        /*
         * sys_language_uid
         */
        
        $iSysLanguageUid = $this->getSysLanguageUid();
        $iCurrentPageUid = $this->getCurrentPageUid();
        /*
         * API token
         */
        
        /*
         * get request params
         */
        
        $aRequestArguments = $this->request->getArguments();
        $aApiToken = $this->getApiToken();
        /*
         * API version
         */
        
        $aApiVersion = $this->getApiVersion();
        /*
         * API typeNum
         */
        
        $aApiTypeNum = self::API_TYPE_NUM;
        /*
         * cache identifier prefix
         */
        
        $sCacheIdentifierPrefix = self::CACHE_IDENTIFIER_PREFIX_RENDER_LIST;
        /*
         * URL
         */

        $iMaxItems = $aSettings['production']['api']['query']['files']['maxItems'];
        $iItemsPerPage = $aSettings['production']['api']['query']['files']['itemsPerPage'];
        $sFilterKey = $aSettings['production']['api']['query']['filterKey'];
        $aFilterKey = explode(',', $sFilterKey);
        $pluginConfiguration = array(
            'pluginName' => self::REQUEST_PLUGIN,
            'controller' => self::REQUEST_CONTROLLER,
            'action' => self::REQUEST_ACTION
        );
        /*
         * plain filter
         */
        
        $aFilter = array(
            'version' => (string) $aApiVersion,
            'get' => 'files',
        );

        /*
         * signalSlotDispatcher
         */
        $this->signalSlotDispatcher->dispatch(
            __CLASS__,
            __FUNCTION__ . 'BeforeSanitizeRequestArguments',
            [&$aRequestArguments]
        );


        /*
         * additional filter values
         */
        
        $aRequestArgumentsSanitized = $this->sanitizeRequestArguments($aFilterKey, $aRequestArguments);
        if (count($aRequestArgumentsSanitized) > 0) {
            foreach ($aRequestArgumentsSanitized as $sKey => $aValue) {
                $aFilter[$sKey] = $aValue;
            }
        }

        $typolink = array(
            'parameter' => $iRequestPageUid,
            'useCacheHash' => TRUE,
            'forceAbsoluteUrl' => ((array_key_exists('HOSTNAME',$_SERVER) && strpos($_SERVER['HOSTNAME'], '.') === false) ? FALSE : TRUE),
            'additionalParams' => '&' . http_build_query(array(
                    'type' => $aApiTypeNum,
                    'token' => $aApiToken,
                    $pluginConfiguration['pluginName'] => $aFilter
                ))
        );
        $this->cObj = $this->configurationManager->getContentObject();
        $sUrl = ((array_key_exists('HOSTNAME',$_SERVER) && strpos($_SERVER['HOSTNAME'], '.') === false) ? 'http://' . $_SERVER['HOSTNAME'] . '/' : '') . $this->cObj->typolink_URL($typolink);
//        var_dump($sUrl);
        $sCacheIdentifier = $sCacheIdentifierPrefix . '_' . $this->getApiVersion() . '_' . $iCurrentPageUid . '_' . $iSysLanguageUid . '_' . 'files';
        if (count($aRequestArgumentsSanitized) > 0) {
            $sCacheIdentifier .= '_' . md5(serialize($aRequestArgumentsSanitized));
        }
        /*
         * Try to get the cached content.
         * If there is content in cache, just return it.
         * Otherwise generate the content, save in cache and then return it.
         *
         * http://lbrmedia.net/codebase/Eintrag/extbase-6-eigenen-cache-in-extension-verwenden/
         * https://docs.typo3.org/typo3cms/CoreApiReference/CachingFramework/Developer/Index.html
         */
        
        $sContent = $this->getCache($sCacheIdentifier);
        if ($sContent === FALSE) {
            /*
             * Create GuzzleHttp\Client
             */
            
            $client = new Client();
            $response = '';
            /*
             * request the API v1700105
             */
            
            try {    //              $sUrl = 'http://hevrhrbhgbhj.com/';
                $response = $client->request('GET', $sUrl);
            } catch (RequestException $e) {    $aBody['bError'] = 1;
                $aBody['sDescription'] = array(
                    'message' => 'Ooops! ' . $e->getMessage(),
                    'code' => 'Request Exeption'
                );
            }
            if ($aBody['bError'] == 0) {
                /*
                 * Check StatusCode
                 */
                
                $iStatusCode = $response->getStatusCode();
                if ($iStatusCode == 200) {
                    $jBody = $response->getBody();
                    $aBody['aApiResponse'] = json_decode($jBody, true);
                    /*
                     * get/render content
                     */
                    
                    $sContent = $this->listActionContent($aBody, $aRequestArguments, $aRequestArgumentsSanitized, $aSettings);
                    $this->setCache($sCacheIdentifier, $sContent, $aSettings);
                } else {
                    $aBody['bError'] = 1;
                    $aBody['sDescription'] = array(
                        'message' => 'Ooops! ' . $iStatusCode,
                        'code' => 'Request Exeption'
                    );
                    $sContent = $this->listActionContent($aBody, $aRequestArguments, $aRequestArgumentsSanitized, $aSettings);
                }
            }
        }
        return $sContent;
    }
    
    /**
     * @param $aBody
     * @param $aRequestArgumentsSanitized
     */
    private function listActionContent($aBody = [], $aRequestArguments = [], $aRequestArgumentsSanitized = [], $aSettings = [])
    {
        /*
         * aHtml
         */
        $aHtml = array(
            'Files' => array(),
            'Pagination' => array(),
            'Count' => ''
        );
        $this->overrideApiResponseFilesForFrontend($aBody['aApiResponse']['Api']['Dlc']['Files']);
        foreach ($aBody['aApiResponse']['Api']['Dlc']['Files']['Files'] as $aFile) {
            $aHtml['Files'][md5($aFile['sys_category__title'])][] = $aFile;
        }
        $aHtml = array_values($aHtml['Files']);
        $aBody['aHtml']['Files'] = $aHtml;

        /*
         * Pagination
         */
        
        foreach ($aBody['aApiResponse']['Api']['Dlc']['Files']['Pagination'] as $aPageLink) {
            $pluginConfiguration = array(
                'pluginName' => self::PRE_REQUEST_PLUGIN,
                'controller' => 'Render',
                'action' => 'list'
            );
            /*
             * plain filter
             */
            
            $aFilter = array(
                'version' => (string) $this->getApiVersion(),
                'get' => 'files'
            );
            /*
             * additional filter values
             */
            
            if (count($aRequestArgumentsSanitized) > 0) {
                foreach ($aRequestArgumentsSanitized as $sKey => $aValue) {
                    $aFilter[$sKey] = $aValue;
                }
            }

            if (is_array($aRequestArguments)
                && array_key_exists('sys_file_metadata_pages_mm', $aRequestArguments)) {

                if (is_array($aRequestArguments['sys_file_metadata_pages_mm'])
                    &&  array_key_exists('mm', $aRequestArguments['sys_file_metadata_pages_mm'])) {

                    $sFilterPagesType = $aSettings['production']['api']['query']['filter']['pages']['type'];
                    if (is_array($aRequestArguments['sys_file_metadata_pages_mm']['mm'])
                        && array_key_exists($sFilterPagesType, $aRequestArguments['sys_file_metadata_pages_mm']['mm'])) {

                        if (is_array($aRequestArguments['sys_file_metadata_pages_mm']['mm'][$sFilterPagesType])) {

                            $aR = $aRequestArguments['sys_file_metadata_pages_mm']['mm'][$sFilterPagesType];
                            $aR = array_filter($aR, 'strlen');

                            if (is_array($aR) && count($aR) > 0) {
                                unset($aFilter['sys_file_metadata_pages_mm']['mm']);
                                foreach ($aR as $iK => $sV) {
                                    $aFilter['sys_file_metadata_pages_mm']['mm'][$sFilterPagesType][$iK] = $sV;
                                }
                            }
                        }
                    }
                }
            }

            $aFilter['page'] = $aPageLink[0];
            $typolink = array(
                'parameter' => $this->getCurrentPageUid(),
                'useCacheHash' => FALSE,
                'forceAbsoluteUrl' => TRUE,
                'additionalParams' => '&' . http_build_query(array(
                        $pluginConfiguration['pluginName'] => $aFilter
                    ))
            );
            $this->cObj = $this->configurationManager->getContentObject();
            $sUrl = $this->cObj->typolink_URL($typolink);
            $aBody['aHtml']['Pagination'][] = array(
                0 => $aPageLink[0],
                1 => $aPageLink[1],
                2 => $sUrl
            );
        }
        /*
         * Count
         */
        
        $aBody['aHtml']['Count'] = $aBody['aApiResponse']['Api']['Dlc']['Files']['Count'];
        $this->view->assign('bDebug', self::B_DEBUG);
        $this->view->assign('aBody', $aBody);
        return $this->view->render();
    }
    
    /**
     * @param $aApiResponseFiles
     */
    private function overrideApiResponseFilesForFrontend(&$aApiResponseFiles)
    {
        foreach ($aApiResponseFiles['Files'] as $iKey => $aFile) {
            if ($aFile['sys_category__title'] == self::PLACE_AT_END_OF_LIST) {
                $aApiResponseFiles['Files'][$iKey]['sys_category__title'] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                    'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation.xlf:tx_teufelsextdlc_domain_model_render.sys_category__title.default',
                    'teufels_ext_dlc'
                );
            } else {
                /*
                 * Localize Categories
                 */
                
                foreach ($aApiResponseFiles['Categories'] as $aCategory) {
                    if (intval($aCategory['uid']) == intval($aFile['sys_category__uid'])) {
                        $aApiResponseFiles['Files'][$iKey]['sys_category__title'] = $aCategory['title'];
                    }
                }
            }
            if ($aFile['sys_file_metadata__title'] == '') {
                $aApiResponseFiles['Files'][$iKey]['sys_file_metadata__title'] = $aFile['sys_file__name'];
            }
            if ($aFile['sys_file__size'] > 0) {
                $aApiResponseFiles['Files'][$iKey]['sys_file__size'] = \TYPO3\CMS\Core\Utility\GeneralUtility::formatSize($aFile['sys_file__size'], '| kB| MB| GB| TB| PB| EB| ZB| YB');
            }
        }
        /*
         * Sort again via
         * sys_category__title ASC
         *  sys_file_metadata__sortkey ASC
         *
         * see http://www.karlvalentin.de/660/mehrdimensionale-arrays-in-php-bequem-sortieren.html
         */
        
        if ($this->getSysLanguageUid() != 0) {
            /*
             * Gemeinerweise benötigt array_multisort ein Array von Spalten, wir aber haben ein Array von Zeilen. Folglich müssen wir erst die relevanten Spalten extrahieren.
             */
            
            foreach ($aApiResponseFiles['Files'] as $key => $row) {
                $sys_category__title[$key] = $row['sys_category__title'];
                $sys_file_metadata__sortkey[$key] = $row['sys_file_metadata__sortkey'];
            }
            /*
             * Jetzt können wir endlich sortieren.
             */
            
            array_multisort($sys_category__title, SORT_ASC, SORT_STRING, $sys_file_metadata__sortkey, SORT_ASC, $aApiResponseFiles['Files']);
        }
    }
    
    /**
     * action show
     *
     * @param \TEUFELS\TeufelsExtDlc\Domain\Model\Render $render
     * @return void
     */
    public function showAction(\TEUFELS\TeufelsExtDlc\Domain\Model\Render $render)
    {
        $this->view->assign('render', $render);
    }
    
    /**
     * action filter
     *
     * @return void
     */
    public function filterAction()
    {
        /*
         * aBody
         */

        $aSettings = $this->getSettingsArray();

        /*
         * get request params
         */

        $aRequestArguments = $this->request->getArguments();

        $aApiToken = $this->getApiToken();
        /*
         * API version
         */

        $aApiVersion = $this->getApiVersion();
        /*
         * API typeNum
         */

        $aApiTypeNum = self::API_TYPE_NUM;
        /*
         * cache identifier prefix
         */
        
        $aBody = array();
        $aBody['bError'] = 0;
        $aBody['sDescription'] = array();
        $aBody['aFilter'] = array();
        $aBody['aDebug'] = array();
        $typolink = array(
            'parameter' => $this->getCurrentPageUid(),
            'useCacheHash' => TRUE,
            'forceAbsoluteUrl' => FALSE
        );

        $sFilterKey = $aSettings['production']['api']['query']['filterKey'];
        $aFilterKey = explode(',', $sFilterKey);
        $aRequestArgumentsSanitized = $this->sanitizeRequestArguments($aFilterKey, $aRequestArguments, true);
//        if (count($aRequestArgumentsSanitized) > 0) {
//            /*
//             * plain filter
//             */
//            $aFilter = array(
//                'version' => (string) $aApiVersion,
//                'get' => 'files',
//            );
//            foreach ($aRequestArgumentsSanitized as $sKey => $aValue) {
//                $aFilter[$sKey] = $aValue;
//            }
//
//            $pluginConfiguration = array(
//                'pluginName' => self::REQUEST_PLUGIN,
//                'controller' => self::REQUEST_CONTROLLER,
//                'action' => self::REQUEST_ACTION
//            );
//            $typolink['additionalParams'] = '&' . http_build_query(array(
//                $pluginConfiguration['pluginName'] => $aFilter
//            ));
//        }

        $sPidList = $aSettings['production']['api']['query']['pages']['pidList'];
        $iLevels = $aSettings['production']['api']['query']['pages']['levels'];
        $aPagetree = $this->apiRepository->apiCallPagesByPid($sPidList, $aSettings);
        $aOptions = array();

        $aSelect[0] = array(
            'name' => "sys_file_metadata_pages_mm_0",
            'id' => "sys_file_metadata_pages_mm_0",
            'options' => array(),
            'selected' => '',
        );

        if (count($aPagetree) > 0) {
            /*
             * for each pidList entry
             */
            foreach ($aPagetree as $aPages) {
                /*
                 * for each page
                 */
                if (is_array($aPages)) {
                    foreach ($aPages as $aPage) {
                        if ($iLevels >= 1) {
                            $aOptions[0][$aPage['uid']] = $aPage['nav_title'] == "" ? $aPage['title'] : $aPage['nav_title'];
                        }
                    }
                }
            }
            $aSelect[0]['options'] = $aOptions[0];

            if (count($aRequestArguments) > 0){
                if (array_key_exists('sys_file_metadata_pages_mm_0', $aRequestArguments)) {
                    $aMm = intval($aRequestArguments['sys_file_metadata_pages_mm_0']);
                    if (array_key_exists($aMm, $aOptions[0])) {
                        $aSelect[0]['selected'] = $aMm;
                    }
                }
            }
        }

        if ($iLevels >= 2) {
            $aSelect[1] = array(
                'name' => "sys_file_metadata_pages_mm_1",
                'id' => "sys_file_metadata_pages_mm_1",
                'options' => array(),
                'selected' => '',
            );

            if ($aSelect[0]['selected'] != '') {
                $aPagetree = $this->apiRepository->apiCallPagesByPid($aSelect[0]['selected'], $aSettings);
                if (count($aPagetree) > 0) {
                    /*
                 * for each pidList entry
                 */
                    foreach ($aPagetree as $aPages) {
                        /*
                         * for each page
                         */
                        if (is_array($aPages)) {
                            foreach ($aPages as $aPage) {
                                $aOptions[1][$aPage['uid']] = $aPage['nav_title'] == "" ? $aPage['title'] : $aPage['nav_title'];
                            }
                        }
                    }
                    $aSelect[1]['options'] = $aOptions[1];

                    if (count($aRequestArguments) > 0){
                        if (array_key_exists('sys_file_metadata_pages_mm_1', $aRequestArguments)) {
                            $aMm = intval($aRequestArguments['sys_file_metadata_pages_mm_1']);
                            if (array_key_exists($aMm, $aOptions[1])) {
                                $aSelect[1]['selected'] = $aMm;
                            }
                        }
                    }
                }
            }
        }

        if ($iLevels >= 3) {
            $aSelect[2] = array(
                'name' => "sys_file_metadata_pages_mm_2",
                'id' => "sys_file_metadata_pages_mm_2",
                'options' => array(),
                'selected' => '',
            );

            if ($aSelect[1]['selected'] != '') {
                $aPagetree = $this->apiRepository->apiCallPagesByPid($aSelect[1]['selected'], $aSettings);
                if (count($aPagetree) > 0) {
                    /*
                 * for each pidList entry
                 */
                    foreach ($aPagetree as $aPages) {
                        /*
                         * for each page
                         */
                        if (is_array($aPages)) {
                            foreach ($aPages as $aPage) {
                                $aOptions[2][$aPage['uid']] = $aPage['nav_title'] == "" ? $aPage['title'] : $aPage['nav_title'];
                            }
                        }
                    }
                    $aSelect[2]['options'] = $aOptions[2];

                    if (count($aRequestArguments) > 0){
                        if (array_key_exists('sys_file_metadata_pages_mm_2', $aRequestArguments)) {
                            $aMm = intval($aRequestArguments['sys_file_metadata_pages_mm_2']);
                            if (array_key_exists($aMm, $aOptions[2])) {
                                $aSelect[2]['selected'] = $aMm;
                            }
                        }
                    }

                }
            }
        }

        if ($iLevels >= 4) {
            $aSelect[3] = array(
                'name' => "sys_file_metadata_pages_mm_3",
                'id' => "sys_file_metadata_pages_mm_3",
                'options' => array(),
                'selected' => '',
            );

            if ($aSelect[2]['selected'] != '') {
                $aPagetree = $this->apiRepository->apiCallPagesByPid($aSelect[2]['selected'], $aSettings);
                if (count($aPagetree) > 0) {
                    /*
                 * for each pidList entry
                 */
                    foreach ($aPagetree as $aPages) {
                        /*
                         * for each page
                         */
                        if (is_array($aPages)) {
                            foreach ($aPages as $aPage) {
                                $aOptions[3][$aPage['uid']] = $aPage['nav_title'] == "" ? $aPage['title'] : $aPage['nav_title'];
                            }
                        }
                    }
                    $aSelect[3]['options'] = $aOptions[3];

                    if (count($aRequestArguments) > 0){
                        if (array_key_exists('sys_file_metadata_pages_mm_3', $aRequestArguments)) {
                            $aMm = intval($aRequestArguments['sys_file_metadata_pages_mm_3']);
                            if (array_key_exists($aMm, $aOptions[3])) {
                                $aSelect[3]['selected'] = $aMm;
                            }
                        }
                    }

                }
            }
        }

//        if (count($aRequestArguments) > 0) {
//            if (array_key_exists('sys_file_metadata_pages_mm_0', $aRequestArguments)) {
//                $aMm = intval($aRequestArguments['sys_file_metadata_pages_mm_0']);
//                if (array_key_exists($aMm, $aOptions[0])) {
//                    $aSelect[0]['selected'] = $aMm;
//
//                    if ($iLevels >= 2) {
//
//
//                    }
//                }
//            }
//        }





        $this->cObj = $this->configurationManager->getContentObject();
        $sUrl = $this->cObj->typolink_URL($typolink);
        $aBody['aFilter']['sUrl'] = $sUrl;
        $aBody['aFilter']['aInput'] = $aSelect;
        $aBody['aDebug']['aPageTree'] = $aPagetree;
        $this->view->assign('bDebug', self::B_DEBUG);
        $this->view->assign('aBody', $aBody);
    }
    
    /**
     * action listOnCurrentPage
     *
     * @return mixed
     */
    public function listOnCurrentPageAction()
    {
        /*
         * aBody
         */
        
        $aBody = array();
        $aBody['bError'] = 0;
        $aBody['sDescription'] = array();
        $aBody['aApiResponse'] = array();
        /*
         * Settings
         */
        
        $aSettings = $this->getSettingsArray();
        /*
         * get page id of request action
         */
        
        $iRequestPageUid = $this->getRequestPageUid();
        /*
         * sys_language_uid
         */
        
        $iSysLanguageUid = $this->getSysLanguageUid();
        $iCurrentPageUid = $this->getCurrentPageUid();
        /*
         * API token
         */
        
        $aApiToken = $this->getApiToken();
        /*
         * API version
         */
        
        $aApiVersion = $this->getApiVersion();
        /*
         * API typeNum
         */
        
        $aApiTypeNum = self::API_TYPE_NUM;
        /*
         * cache identifier prefix
         */
        
        $sCacheIdentifierPrefix = self::CACHE_IDENTIFIER_PREFIX_RENDER_LISTONCURRENTPAGE;
        /*
         * URL
         */
        
        $pluginConfiguration = array(
            'pluginName' => self::REQUEST_PLUGIN,
            'controller' => self::REQUEST_CONTROLLER,
            'action' => self::REQUEST_ACTION
        );
        $typolink = array(
            'parameter' => $iRequestPageUid,
            'useCacheHash' => TRUE,
            'forceAbsoluteUrl' => ((array_key_exists('HOSTNAME',$_SERVER) && strpos($_SERVER['HOSTNAME'], '.') === false) ? FALSE : TRUE),
            'additionalParams' => '&' . http_build_query(array(
                    'type' => $aApiTypeNum,
                    'token' => $aApiToken,
                    $pluginConfiguration['pluginName'] => array(
                        'version' => (string) $aApiVersion,
                        'get' => 'files',
                        'sys_file_metadata_pages_mm' => array(
                            'mm' => $iCurrentPageUid
                        ),
                        'itemsPerPage' => 300
                    )
                ))
        );
        $this->cObj = $this->configurationManager->getContentObject();
        $sUrl = ((array_key_exists('HOSTNAME',$_SERVER) && strpos($_SERVER['HOSTNAME'], '.') === false) ? 'http://' . $_SERVER['HOSTNAME'] . '/' : '') . $this->cObj->typolink_URL($typolink);
        /*
         * calculate the identifier for the cached entry
         * Use "request_request" cause we are caching this value in filter
         */
        
        $sCacheIdentifier = $sCacheIdentifierPrefix . '_' . $aApiVersion . '_' . $iCurrentPageUid . '_' . $iSysLanguageUid . '_' . 'files';
        /*
         * Try to get the cached content.
         * If there is content in cache, just return it.
         * Otherwise generate the content, save in cache and then return it.
         *
         * http://lbrmedia.net/codebase/Eintrag/extbase-6-eigenen-cache-in-extension-verwenden/
         * https://docs.typo3.org/typo3cms/CoreApiReference/CachingFramework/Developer/Index.html
         */
        
        $sContent = $this->getCache($sCacheIdentifier);
        if ($sContent === FALSE) {
            /*
             * Create GuzzleHttp\Client
             */
            
            $client = new Client();
            $response = '';
            /*
             * request the API v1700105
             */
            
            try {    //              $sUrl = 'http://hevrhrbhgbhj.com/';
                $response = $client->request('GET', $sUrl);
            } catch (RequestException $e) {    $aBody = array(
                    'bError' => 1,
                    'sDescription' => array(
                        'message' => 'Ooops! ' . $e->getMessage(),
                        'code' => 'Request Exeption'
                    )
                );
            }
            if ($aBody['bError'] == 0) {
                /*
                 * Check StatusCode
                 */
                
                $iStatusCode = $response->getStatusCode();
                if ($iStatusCode == 200) {
                    $jBody = $response->getBody();
                    $aBody = array(
                        'bError' => 0,
                        'sDescription' => array(),
                        'aApiResponse' => json_decode($jBody, true)
                    );
                    /*
                     * get/render content
                     */
                    
                    $sContent = $this->listOnCurrentPageActionContent($aBody);
                    $this->setCache($sCacheIdentifier, $sContent, $aSettings);
                } else {
                    $aBody = array(
                        'bError' => 1,
                        'sDescription' => array(
                            'message' => 'Ooops! ' . $iStatusCode,
                            'code' => 'Request Exeption'
                        )
                    );
                    $sContent = $this->listOnCurrentPageActionContent($aBody);
                }
            }
        }
        return $sContent;
    }
    
    /**
     * @param $aBody
     */
    private function listOnCurrentPageActionContent($aBody = array())
    {
        /*
         * aHtml
         */
        
        $aHtml = array();
        //        foreach ($aBody['aApiResponse']['Api']['Dlc']['Files']['Files'] as $aFile) {
        //            if ($aFile['sys_category__title'] == self::PLACE_AT_END_OF_LIST) {
        //                $aFile['sys_category__title'] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
        //                    'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation.xlf:tx_teufelsextdlc_domain_model_render.sys_category__title.default',
        //                    'teufels_ext_dlc'
        //                );
        //            }
        //            if ($aFile['sys_file_metadata__title'] == '') {
        //                $aFile['sys_file_metadata__title'] = $aFile['sys_file__name'];
        //            }
        //            if ($aFile['sys_file__size'] > 0) {
        //                $aFile['sys_file__size'] = \TYPO3\CMS\Core\Utility\GeneralUtility::formatSize($aFile['sys_file__size'], '| kB| MB| GB| TB| PB| EB| ZB| YB');
        //            }
        //            $aHtml[md5($aFile['sys_category__title'])][] = $aFile;
        //        }
        //        $aHtml = array_values($aHtml);
        //        $aBody['aHtml'] = $aHtml;
        $this->overrideApiResponseFilesForFrontend($aBody['aApiResponse']['Api']['Dlc']['Files']);
        foreach ($aBody['aApiResponse']['Api']['Dlc']['Files']['Files'] as $aFile) {
            $aHtml[md5($aFile['sys_category__title'])][] = $aFile;
        }
        $aHtml = array_values($aHtml);
        $aBody['aHtml'] = $aHtml;
        $this->view->assign('bDebug', self::B_DEBUG);
        $this->view->assign('aBody', $aBody);
        return $this->view->render();
    }
    
    /**
     * action listCategories
     *
     * @return mixed
     */
    public function listCategoriesAction()
    {
        /*
         * aBody
         */
        
        $aBody = array();
        $aBody['bError'] = 0;
        $aBody['sDescription'] = array();
        $aBody['aApiResponse'] = array();
        /*
         * Settings
         */
        
        $aSettings = $this->getSettingsArray();
        /*
         * get request params
         */

        $aRequestArguments = $this->request->getArguments();

        /*
         * get page id of request action
         */
        
        $iRequestPageUid = $this->getRequestPageUid();
        /*
         * sys_language_uid
         */
        
        $iSysLanguageUid = $this->getSysLanguageUid();
        $iCurrentPageUid = $this->getCurrentPageUid();
        /*
         * API token
         */
        
        $aApiToken = $this->getApiToken();
        /*
         * API version
         */
        
        $aApiVersion = $this->getApiVersion();
        /*
         * API typeNum
         */
        
        $aApiTypeNum = self::API_TYPE_NUM;
        /*
         * cache identifier prefix
         */
        
        $sCacheIdentifierPrefix = self::CACHE_IDENTIFIER_PREFIX;
        /*
         * URL
         */
        
        $pluginConfiguration = array(
            'pluginName' => self::REQUEST_PLUGIN,
            'controller' => self::REQUEST_CONTROLLER,
            'action' => self::REQUEST_ACTION
        );
        $typolink = array(
            'parameter' => $iRequestPageUid,
            'useCacheHash' => TRUE,
            'forceAbsoluteUrl' => ((array_key_exists('HOSTNAME',$_SERVER) && strpos($_SERVER['HOSTNAME'], '.') === false) ? FALSE : TRUE),
            'additionalParams' => '&' . http_build_query(array(
                    'type' => $aApiTypeNum,
                    'token' => $aApiToken,
                    $pluginConfiguration['pluginName'] => array(
                        'version' => (string) $aApiVersion,
                        'get' => 'categories'
                    )
                ))
        );
        $this->cObj = $this->configurationManager->getContentObject();
        $sUrl = ((array_key_exists('HOSTNAME',$_SERVER) && strpos($_SERVER['HOSTNAME'], '.') === false) ? 'http://' . $_SERVER['HOSTNAME'] . '/' : '') . $this->cObj->typolink_URL($typolink);
        /*
         * calculate the identifier for the cached entry
         * Use "request_request" cause we are caching this value in filter
         */
        
        $sCacheIdentifier = $sCacheIdentifierPrefix . '_categories_' . $iCurrentPageUid . '_' . $iSysLanguageUid;
        /*
         * Try to get the cached content.
         * If there is content in cache, just return it.
         * Otherwise generate the content, save in cache and then return it.
         *
         * http://lbrmedia.net/codebase/Eintrag/extbase-6-eigenen-cache-in-extension-verwenden/
         * https://docs.typo3.org/typo3cms/CoreApiReference/CachingFramework/Developer/Index.html
         */
        
        $sContent = $this->getCache($sCacheIdentifier);
        if ($sContent === FALSE) {
            /*
             * Create GuzzleHttp\Client
             */
            
            $client = new Client();
            $response = '';
            /*
             * request the API v1700105
             */
            
            try {    //              $sUrl = 'http://hevrhrbhgbhj.com/';
                $response = $client->request('GET', $sUrl);
            } catch (RequestException $e) {    $aBody = array(
                    'bError' => 1,
                    'sDescription' => array(
                        'message' => 'Ooops! ' . $e->getMessage(),
                        'code' => 'Request Exeption'
                    )
                );
            }
            if ($aBody['bError'] == 0) {
                /*
                 * Check StatusCode
                 */
                
                $iStatusCode = $response->getStatusCode();
                if ($iStatusCode == 200) {
                    $jBody = $response->getBody();
                    $aBody = array(
                        'bError' => 0,
                        'sDescription' => array(),
                        'aApiResponse' => json_decode($jBody, true)
                    );
                    /*
                     * get/render content
                     */
                    
                    $sContent = $this->listCategoriesActionContent($aBody, $aRequestArguments);
                } else {
                    $aBody = array(
                        'bError' => 1,
                        'sDescription' => array(
                            'message' => 'Ooops! ' . $iStatusCode,
                            'code' => 'Request Exeption'
                        )
                    );
                    $sContent = $this->listCategoriesActionContent($aBody, $aRequestArguments);
                }
            }
        }
        return $sContent;
    }
    
    /**
     * @param $aBody
     */
    private function listCategoriesActionContent($aBody = array(), $aRequestArguments = array())
    {
        /*
         * aHtml
         */

        $aKey = array('sys_category_record_mm','mm');

        $aOptions = array();
        foreach ($aBody['aApiResponse']['Api']['Dlc']['Categories'] as $aCategory) {
            $aOptions[$aCategory['uid']] = $aCategory['title'];
        }
        $aBody['aHtml'] = array(
            'name' => self::LIST_PLUGIN . '[' . $aKey[0] . '][' . $aKey[1] . ']',
            'id' => 'sys_category_record_mm',
            'options' => $aOptions,
            'selected' => '',
        );

        if (count($aRequestArguments) > 0){
            if (array_key_exists($aKey[0], $aRequestArguments)) {
                if (array_key_exists($aKey[1], $aRequestArguments[$aKey[0]])) {
                    $aMm = intval($aRequestArguments[$aKey[0]][$aKey[1]]);
                }
                if (array_key_exists($aMm, $aOptions)) {
                    $aBody['aHtml']['selected'] = $aMm;
                }
            }
        }

        $this->view->assign('bDebug', self::B_DEBUG);
        $this->view->assign('aBody', $aBody);
        return $this->view->render();
    }
    
    /**
     * action listLanguages
     *
     * @return mixed
     */
    public function listLanguagesAction()
    {
        /*
         * aBody
         */
        
        $aBody = array();
        $aBody['bError'] = 0;
        $aBody['sDescription'] = array();
        $aBody['aApiResponse'] = array();
        /*
         * Settings
         */
        
        $aSettings = $this->getSettingsArray();
        /*
         * get page id of request action
         */
        
        $iRequestPageUid = $this->getRequestPageUid();
        /*
         * sys_language_uid
         */
        
        $iSysLanguageUid = $this->getSysLanguageUid();
        $iCurrentPageUid = $this->getCurrentPageUid();
        /*
         * API token
         */
        
        $aApiToken = $this->getApiToken();
        /*
         * API version
         */
        
        $aApiVersion = $this->getApiVersion();
        /*
         * API typeNum
         */
        
        $aApiTypeNum = self::API_TYPE_NUM;
        /*
         * cache identifier prefix
         */
        
        $sCacheIdentifierPrefix = self::CACHE_IDENTIFIER_PREFIX;
        /*
         * URL
         */
        
        $pluginConfiguration = array(
            'pluginName' => self::REQUEST_PLUGIN,
            'controller' => self::REQUEST_CONTROLLER,
            'action' => self::REQUEST_ACTION
        );
        $typolink = array(
            'parameter' => $iRequestPageUid,
            'useCacheHash' => TRUE,
            'forceAbsoluteUrl' => ((array_key_exists('HOSTNAME',$_SERVER) && strpos($_SERVER['HOSTNAME'], '.') === false) ? FALSE : TRUE),
            'additionalParams' => '&' . http_build_query(array(
                    'type' => $aApiTypeNum,
                    'token' => $aApiToken,
                    $pluginConfiguration['pluginName'] => array(
                        'version' => (string) $aApiVersion,
                        'get' => 'languages'
                    )
                ))
        );
        $this->cObj = $this->configurationManager->getContentObject();
        $sUrl = ((array_key_exists('HOSTNAME',$_SERVER) && strpos($_SERVER['HOSTNAME'], '.') === false) ? 'http://' . $_SERVER['HOSTNAME'] . '/' : '') . $this->cObj->typolink_URL($typolink);
        /*
         * calculate the identifier for the cached entry
         * Use "request_request" cause we are caching this value in filter
         */
        
        $sCacheIdentifier = $sCacheIdentifierPrefix . '_languages_' . $iCurrentPageUid . '_' . $iSysLanguageUid;
        /*
         * Try to get the cached content.
         * If there is content in cache, just return it.
         * Otherwise generate the content, save in cache and then return it.
         *
         * http://lbrmedia.net/codebase/Eintrag/extbase-6-eigenen-cache-in-extension-verwenden/
         * https://docs.typo3.org/typo3cms/CoreApiReference/CachingFramework/Developer/Index.html
         */
        
        $sContent = $this->getCache($sCacheIdentifier);
        if ($sContent === FALSE) {
            /*
             * Create GuzzleHttp\Client
             */
            
            $client = new Client();
            $response = '';
            /*
             * request the API v1700105
             */
            
            try {    //              $sUrl = 'http://hevrhrbhgbhj.com/';
                $response = $client->request('GET', $sUrl);
            } catch (RequestException $e) {    $aBody = array(
                    'bError' => 1,
                    'sDescription' => array(
                        'message' => 'Ooops! ' . $e->getMessage(),
                        'code' => 'Request Exeption'
                    )
                );
            }
            if ($aBody['bError'] == 0) {
                /*
                 * Check StatusCode
                 */
                
                $iStatusCode = $response->getStatusCode();
                if ($iStatusCode == 200) {
                    $jBody = $response->getBody();
                    $aBody = array(
                        'bError' => 0,
                        'sDescription' => array(),
                        'aApiResponse' => json_decode($jBody, true)
                    );
                    /*
                     * get/render content
                     */
                    
                    $sContent = $this->listLanguagesActionContent($aBody);
                } else {
                    $aBody = array(
                        'bError' => 1,
                        'sDescription' => array(
                            'message' => 'Ooops! ' . $iStatusCode,
                            'code' => 'Request Exeption'
                        )
                    );
                    $sContent = $this->listLanguagesActionContent($aBody);
                }
            }
        }
        return $sContent;
    }
    
    /**
     * @param $aBody
     */
    private function listLanguagesActionContent($aBody = array())
    {
        /*
         * aHtml
         */
        
        $aHtml = array();
        foreach ($aBody['aApiResponse']['Api']['Dlc']['Languages'] as $aLanguage) {
            $aHtml[$aLanguage['uid']] = $aLanguage['iso6391'] . '_' . $aLanguage['iso31661'];
        }
        $aBody['aHtml'] = array(
            'name' => self::REQUEST_PLUGIN . '[sys_file_metadata_sys_language_mm][mm]',
            'options' => $aHtml
        );
        $this->view->assign('bDebug', self::B_DEBUG);
        $this->view->assign('aBody', $aBody);
        return $this->view->render();
    }
    
    /**
     * action listPages
     *
     * @return mixed
     */
    public function listPagesAction()
    {
        /*
         * aBody
         */
        
        $aBody = array();
        $aBody['bError'] = 0;
        $aBody['sDescription'] = array();
        $aBody['aApiResponse'] = array();
        /*
         * Settings
         */
        
        $aSettings = $this->getSettingsArray();
        /*
         * get page id of request action
         */
        
        $iRequestPageUid = $this->getRequestPageUid();
        /*
         * sys_language_uid
         */
        
        $iSysLanguageUid = $this->getSysLanguageUid();
        $iCurrentPageUid = $this->getCurrentPageUid();
        /*
         * API token
         */
        
        $aApiToken = $this->getApiToken();
        /*
         * API version
         */
        
        $aApiVersion = $this->getApiVersion();
        /*
         * API typeNum
         */
        
        $aApiTypeNum = self::API_TYPE_NUM;
        /*
         * cache identifier prefix
         */
        
        $sCacheIdentifierPrefix = self::CACHE_IDENTIFIER_PREFIX;
        /*
         * get request params
         */

        $aRequestArguments = $this->request->getArguments();

        $sFilterKey = $aSettings['production']['api']['query']['filterKey'];
        $aFilterKey = explode(',', $sFilterKey);

        $aSanitizedArguments = $this->sanitizeRequestArguments($aFilterKey, $aRequestArguments);

//        print('$aRequestArguments: ');
//        var_dump($aRequestArguments);
//        print('$aSanitizedArguments: ');
//        var_dump($aSanitizedArguments);

        /*
         * URL
         */
        $pluginConfiguration = array(
            'pluginName' => self::REQUEST_PLUGIN,
            'controller' => self::REQUEST_CONTROLLER,
            'action' => self::REQUEST_ACTION
        );
        $typolink = array(
            'parameter' => $iRequestPageUid,
            'useCacheHash' => TRUE,
            'forceAbsoluteUrl' => ((array_key_exists('HOSTNAME',$_SERVER) && strpos($_SERVER['HOSTNAME'], '.') === false) ? FALSE : TRUE),
            'additionalParams' => '&' . http_build_query(array(
                    'type' => $aApiTypeNum,
                    'token' => $aApiToken,
                    $pluginConfiguration['pluginName'] => array(
                        'version' => (string) $aApiVersion,
                        'get' => 'pages',
                        'pidList' => $aSettings['production']['api']['query']['pages']['pidList']
                    )
                ))
        );
        $this->cObj = $this->configurationManager->getContentObject();
        $sUrl = ((array_key_exists('HOSTNAME',$_SERVER) && strpos($_SERVER['HOSTNAME'], '.') === false) ? 'http://' . $_SERVER['HOSTNAME'] . '/' : '') . $this->cObj->typolink_URL($typolink);
        /*
         * calculate the identifier for the cached entry
         * Use "request_request" cause we are caching this value in filter
         */
        
        $sCacheIdentifier = $sCacheIdentifierPrefix . '_pages_' . $iCurrentPageUid . '_' . $iSysLanguageUid . '_' . md5($aSettings['production']['api']['query']['pages']['pidList']);
        /*
         * Try to get the cached content.
         * If there is content in cache, just return it.
         * Otherwise generate the content, save in cache and then return it.
         *
         * http://lbrmedia.net/codebase/Eintrag/extbase-6-eigenen-cache-in-extension-verwenden/
         * https://docs.typo3.org/typo3cms/CoreApiReference/CachingFramework/Developer/Index.html
         */
        
        $sContent = $this->getCache($sCacheIdentifier);
        if ($sContent === FALSE) {
            /*
             * Create GuzzleHttp\Client
             */
            
            $client = new Client();
            $response = '';
            /*
             * request the API v1700105
             */
            
            try {    //              $sUrl = 'http://hevrhrbhgbhj.com/';
                $response = $client->request('GET', $sUrl);
            } catch (RequestException $e) {    $aBody = array(
                    'bError' => 1,
                    'sDescription' => array(
                        'message' => 'Ooops! ' . $e->getMessage(),
                        'code' => 'Request Exeption'
                    )
                );
            }
            if ($aBody['bError'] == 0) {
                /*
                 * Check StatusCode
                 */
                
                $iStatusCode = $response->getStatusCode();
                if ($iStatusCode == 200) {
                    $jBody = $response->getBody();
                    $aBody = array(
                        'bError' => 0,
                        'sDescription' => array(),
                        'aApiResponse' => json_decode($jBody, true)
                    );
                    /*
                     * get/render content
                     */
                    
                    $sContent = $this->listPagesActionContent($aBody, $aRequestArguments, $aSanitizedArguments, $aSettings);
                } else {
                    $aBody = array(
                        'bError' => 1,
                        'sDescription' => array(
                            'message' => 'Ooops! ' . $iStatusCode,
                            'code' => 'Request Exeption'
                        )
                    );
                    $sContent = $this->listPagesActionContent($aBody, $aRequestArguments, $aSanitizedArguments, $aSettings);
                }
            }
        }
        return $sContent;
    }
    
    /**
     * @param $aBody
     */
    private function listPagesActionContent($aBody = [], $aRequestArguments = [], $aSanitizedArguments = [], $aSettings = [])
    {
        $sFilterPagesType = $aSettings['production']['api']['query']['filter']['pages']['type'];
        $sPidList = $aSettings['production']['api']['query']['pages']['pidList'];
        $iLevels_ = $aSettings['production']['api']['query']['pages']['levels'];
        $iLevels = ($iLevels_ >= 1 && $iLevels_ <= 4 ? $iLevels_ : 1);
        $aPagetree = $this->apiRepository->apiCallPagesByPid($sPidList, $aSettings);
        $aOptionGroups = [];
        $aOptions = [];


        $aKey = [
            'sys_file_metadata_pages_mm',
            'mm'
        ];

        $aSelect = [];

        $aSelect[0] = [
            'name' => self::LIST_PLUGIN . '[' . $aKey[0] . '][' . $aKey[1] . '][' . $sFilterPagesType . '][]',
            'id' => "sys_file_metadata_pages_mm__mm__0",
            'optgroups' => [],
            'options' => [],
            'selected' => '',
        ];

        if (count($aPagetree) > 0) {
            /*
             * for each pidList entry
             */
            foreach ($aPagetree as $aPages) {
                /*
                 * for each page
                 */
                if (is_array($aPages)) {
                    foreach ($aPages as $aPage) {
                        if ($iLevels >= 1) {
                            if (array_key_exists('optgroup',$aPage)) {
                                $aOptionGroups[0][$aPage['uid']] =
                                    [
                                        "sLabel" => $aPage['nav_title'] == "" ? $aPage['title'] : $aPage['nav_title'],
                                        "aOptions" => []
                                    ];
                            } else {
                                $aOptions[0][$aPage['uid']] = ($aPage['nav_title'] == "" ? $aPage['title'] : $aPage['nav_title']);
                                if (array_key_exists($aPage['pid'],$aOptionGroups[0])) {
                                    $aOptionGroups[0][$aPage['pid']]["aOptions"][$aPage['uid']] =
                                        ($aPage['nav_title'] == "" ? $aPage['title'] : $aPage['nav_title']);
                                }
                            }

                        }
                    }
                }
            }
            $aSelect[0]['optgroups'] = $aOptionGroups[0];
            $aSelect[0]['options'] = $aOptions[0];



//            if (is_array($aSanitizedArguments)
//                && count($aSanitizedArguments) > 0
//                && array_key_exists('sys_file_metadata_pages_mm', $aSanitizedArguments))
//            {
//
//                if (is_array($aSanitizedArguments['sys_file_metadata_pages_mm'])
//                    && array_key_exists('mm', $aSanitizedArguments['sys_file_metadata_pages_mm'])
//                    && $aSanitizedArguments['sys_file_metadata_pages_mm']['mm'] != '')
//                {
//
//                    $aMm = explode(",", $aSanitizedArguments['sys_file_metadata_pages_mm']['mm']);
//                    foreach ($aMm as $iMm) {
//                        if (array_key_exists($iMm, $aOptions[0])) {
//                            $aSelect[0]['selected'] = $iMm;
//                        }
//                    }
//                }
//            }

            if (is_array($aRequestArguments)
                && array_key_exists('sys_file_metadata_pages_mm', $aRequestArguments)) {

                if (is_array($aRequestArguments['sys_file_metadata_pages_mm'])
                    &&  array_key_exists('mm', $aRequestArguments['sys_file_metadata_pages_mm'])) {

                    if (is_array($aRequestArguments['sys_file_metadata_pages_mm']['mm'])
                        && array_key_exists($sFilterPagesType, $aRequestArguments['sys_file_metadata_pages_mm']['mm'])) {

                        if (is_array($aRequestArguments['sys_file_metadata_pages_mm']['mm'][$sFilterPagesType])
                            && $aRequestArguments['sys_file_metadata_pages_mm']['mm'][$sFilterPagesType][0] != '') {

                            $aMm = intval($aRequestArguments['sys_file_metadata_pages_mm']['mm'][$sFilterPagesType][0]);
                            if (array_key_exists($aMm, $aOptions[0])) {
                                $aSelect[0]['selected'] = $aMm;
                            }
                        }
                    }
                }
            }

        }

        if ($iLevels >= 2) {
            for ($p = 1; $p < $iLevels; $p++) {

                $aSelect[$p] = [
                    'name' => self::LIST_PLUGIN . '[' . $aKey[0] . '][' . $aKey[1] . '][' . $sFilterPagesType . '][]',
                    'id' => "sys_file_metadata_pages_mm__mm__$p",
                    'optgroups' => [],
                    'options' => [],
                    'selected' => '',
                ];

                if ($aSelect[$p - 1]['selected'] != '') {
                    $aPagetree = $this->apiRepository->apiCallPagesByPid($aSelect[$p - 1]['selected'], $aSettings);
                    if (count($aPagetree) > 0) {
                        /*
                         * for each pidList entry
                         */
                        foreach ($aPagetree as $aPages) {
                            /*
                             * for each page
                             */
                            if (is_array($aPages)) {
                                foreach ($aPages as $aPage) {
                                    $aOptions[$p][$aPage['uid']] = $aPage['nav_title'] == "" ? $aPage['title'] : $aPage['nav_title'];
                                }
                            }
                        }
                        $aSelect[$p]['options'] = $aOptions[$p];

                        if (is_array($aRequestArguments)
                            && array_key_exists('sys_file_metadata_pages_mm', $aRequestArguments)) {

                            if (is_array($aRequestArguments['sys_file_metadata_pages_mm'])
                                &&  array_key_exists('mm', $aRequestArguments['sys_file_metadata_pages_mm'])) {

                                if (is_array($aRequestArguments['sys_file_metadata_pages_mm']['mm'])
                                    && array_key_exists($sFilterPagesType, $aRequestArguments['sys_file_metadata_pages_mm']['mm'])) {

                                    if (is_array($aRequestArguments['sys_file_metadata_pages_mm']['mm'][$sFilterPagesType])
                                        && $aRequestArguments['sys_file_metadata_pages_mm']['mm'][$sFilterPagesType][$p] != '') {

                                        $aMm = intval($aRequestArguments['sys_file_metadata_pages_mm']['mm'][$sFilterPagesType][$p]);
                                        if (array_key_exists($aMm, $aOptions[$p])) {
                                            $aSelect[$p]['selected'] = $aMm;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //$aSelect[$p]['optgroups'] = $aOptionGroups[$p];
            }
        }

        /*
         * aHtml
         */
        $aHtml = [];
        $aBody['aHtml'] = $aSelect;
        $this->view->assign('bDebug', self::B_DEBUG);
        $this->view->assign('aBody', $aBody);
        return $this->view->render();
    }
    
    /**
     * action jsPages
     *
     * @return void
     */
    public function jsPagesAction()
    {
        /*
         * Settings
         */

        $aSettings = $this->getSettingsArray();

        $sPidList = $aSettings['production']['api']['query']['pages']['pidList'];
        $iLevels = $aSettings['production']['api']['query']['pages']['levels'];

        $aResult = $this->apiRepository->apiCallPagesByPidListAndLevels($sPidList, $iLevels, $aSettings);

        $aHtml = "<script type=\"text/javascript\">";
        $aHtml .= "var tx_teufelsextdlc_teufelsextdlcrenderjspages__pages = " . json_encode($aResult, true) . ";";
        $aHtml .= "</script>";
        $this->view->assign('aHtml', $aHtml);

    }

}
<?php
namespace TEUFELS\TeufelsExtDlc\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ApiController
 */
class ApiController extends \TEUFELS\TeufelsExtDlc\Controller\AbstractController
{

//    const VALID_CHARACTERS = '/[^0-9]/';
//
//    const VALID_CHARACTERSFORFIELDS = '/[^0-9A-Za-z]/';
    
    /**
     * apiRepository
     *
     * @var \TEUFELS\TeufelsExtDlc\Domain\Repository\ApiRepository
     * @inject
     */
    protected $apiRepository = NULL;
    
    /**
     * languageRepository
     *
     * @var \TEUFELS\TeufelsExtDlc\Domain\Repository\LanguageRepository
     * @inject
     */
    protected $languageRepository = NULL;
    
    /**
     * @var \TYPO3\CMS\Extbase\Mvc\View\JsonView
     */
    protected $view = null;
    
    /**
     * @var string
     */
    protected $defaultViewObjectName = 'TYPO3\\CMS\\Extbase\\Mvc\\View\\JsonView';
    
    /**
     * action request
     *
     * @return mixed
     */
    public function requestAction()
    {
        $sContent = '';
        /*
         * Settings
         */
        
        $aSettings = $this->getSettingsArray();
        /*
         * get page id of request action
         */
        
        $iRequestPageUid = $this->getRequestPageUid();
        /*
         * sys_language_uid
         */
        
        $iSysLanguageUid = $this->getSysLanguageUid();
        /*
         * get request params
         */
        
        $aRequestArguments = $this->request->getArguments();
        /*
         * cache identifier prefix
         */
        
        $sCacheIdentifierPrefix = self::CACHE_IDENTIFIER_PREFIX;
        //TODO Documentation
        /*
         * sanitize get request params
         */
        
        $aApiResponse = array(
            'Api' => array(
                'Dlc' => array(
                    
                )
            )
        );
        $iMaxItems = $aSettings['production']['api']['query']['files']['maxItems'];
        $iItemsPerPage = $aSettings['production']['api']['query']['files']['itemsPerPage'];
        $sFilterKey = $aSettings['production']['api']['query']['filterKey'];
        $aFilterKey = explode(',', $sFilterKey);
        /*
        *  e.g.
           array(1) {
             ["version"]=> 1
           }
        */
        
        $sCacheIdentifier = '';
        switch ($aRequestArguments['version']) {
            case '1':    $sCacheIdentifier .= $sCacheIdentifierPrefix . '_' . $aRequestArguments['version'] . '_' . $iRequestPageUid . '_' . $iSysLanguageUid . '_';
                switch ($aRequestArguments['get']) {
                    case 'pages':    $aRequestArgumentsSanitized = $this->sanitizeRequestArguments($aFilterKey, $aRequestArguments);
                        $sCacheIdentifier .= $aRequestArguments['get'] . '_' . md5(serialize($aRequestArgumentsSanitized));
                        if (array_key_exists('pidList', $aRequestArgumentsSanitized)) {
                            if ($aRequestArgumentsSanitized['pidList'] != '') {
                                $sContent = $this->getCache($sCacheIdentifier);
                                if ($sContent === FALSE) {
                                    $aResult = $this->apiRepository->apiCallPagesByPid($aRequestArgumentsSanitized['pidList'], $aSettings);
                                    $aApiResponse = array(
                                        'Api' => array(
                                            'Dlc' => array(
                                                'Pages' => $aResult
                                            )
                                        )
                                    );
                                    $this->setCache($sCacheIdentifier, json_encode($aApiResponse, TRUE), $aSettings);
                                    $this->view->assign('value', $aApiResponse);
                                    return $this->view->render();
                                }
                            } else {
                                $sCacheIdentifier = '';
                                $this->response->setStatus(400, 'GET Argument: \'pidList\' must not be empty');
                                $this->view->assign('value', $aApiResponse);
                                return $this->view->render();
                            }
                        } else {
                            $sCacheIdentifier = '';
                            $this->response->setStatus(400, 'GET Missing argument: \'pidList\'');
                            $this->view->assign('value', $aApiResponse);
                            return $this->view->render();
                        }
                        break;
                    case 'categories':    $sCacheIdentifier .= $aRequestArguments['get'];
                        $sContent = $this->getCache($sCacheIdentifier);
                        if ($sContent === FALSE) {
                            $aResult = $this->apiRepository->apiCallCategories(9999, 0, $aSettings);
                            $aApiResponse = array(
                                'Api' => array(
                                    'Dlc' => array(
                                        'Categories' => $aResult
                                    )
                                )
                            );
                            $sContent = $this->renderApiResponse($aApiResponse);
                            $this->setCache($sCacheIdentifier, $sContent, $aSettings);
                        }
                        break;
                    case 'languages':    $sCacheIdentifier .= $aRequestArguments['get'];
                        $sContent = $this->getCache($sCacheIdentifier);
                        if ($sContent === FALSE) {
                            $aResult = $this->apiRepository->apiCallLanguages(9999, 0, $aSettings);
                            $aApiResponse = array(
                                'Api' => array(
                                    'Dlc' => array(
                                        'Languages' => $aResult
                                    )
                                )
                            );
                            $sContent = $this->renderApiResponse($aApiResponse);
                            $this->setCache($sCacheIdentifier, $sContent, $aSettings);
                        }
                        break;
                    case 'files':

                        /*
                         * Language Handling
                         */
                        switch ($this->getSysLanguageUid()) {
                            case 0:
                                /*
                                 * Extrawurscht for language 0,
                                 * because language 0 is not in sys_language table
                                 */

                                $aRequestArguments['show_in_default_language'] = array(
                                    'eq' => 1
                                );
                                break;
                            default:    $aRequestArguments['sys_file_metadata_sys_language_mm'] = array(
                                'mm' => $this->getSysLanguageUid()
                            );
                        }

                        $aRequestArgumentsSanitized = $this->sanitizeRequestArguments($aFilterKey, $aRequestArguments);

                        $sCacheIdentifier .= $aRequestArguments['get'] . '_' . md5(serialize($aRequestArgumentsSanitized));
                        $sContent = $this->getCache($sCacheIdentifier);
                        if ($sContent === FALSE) {
                            /*
                             * Count all files for current filter
                             */
                            
                            $iLimit = $iMaxItems;
                            $iOffset = 0;
                            /*
                             * $bSimple = FALSE, $iLimit = 999, $iOffset = 0, $aFilterMM = array(), $aFilterM1 = array()
                             */
                            
                            $aResponse = $this->apiRepository->apiCallList(FALSE, $iLimit, $iOffset, $aSettings, $aRequestArgumentsSanitized);
                            $iCountResponse = count($aResponse);
                            /*
                             * Result for current filter
                             */
                            
                            $iLimit = array_key_exists('itemsPerPage', $aRequestArgumentsSanitized) ? $aRequestArgumentsSanitized['itemsPerPage'] : $iItemsPerPage;
                            $iMaxDivision = $iCountResponse / $iLimit; //var_dump($iMaxDivision);
                            $iMaxRound = round($iCountResponse / $iLimit); //var_dump($iMaxRound);
                            $iMax = ($iMaxDivision > $iMaxRound ? ($iMaxRound + 1) : $iMaxRound); //var_dump($iMax);
                            if (array_key_exists('page', $aRequestArgumentsSanitized)) {
                                /*
                                 * get offset
                                 */

                                if ($aRequestArgumentsSanitized['page'] <= $iMax) {
                                    $iOffset = $iLimit * ($aRequestArgumentsSanitized['page'] - 1);
                                }
                                if ($aRequestArgumentsSanitized['page'] == 1) {
                                    $iOffset = $iLimit * ($aRequestArgumentsSanitized['page'] - 1);
                                }
                            }
                            $aResponse = $this->apiRepository->apiCallList(FALSE, $iLimit, $iOffset, $aSettings, $aRequestArgumentsSanitized);
                            /*
                             * pagination array
                             */
                            
                            $aPagination = $this->getPaginationArray($aSettings, $aRequestArgumentsSanitized, $iCountResponse, $iLimit);
                            /*
                             * Localized Categories for frontend output
                             */
                            
                            $aResultCategories = $this->apiRepository->apiCallCategories(9999, 0, $aSettings);
                            $aApiResponse = array(
                                'Api' => array(
                                    'Dlc' => array(
                                        'Files' => array(
                                            'Count' => $iCountResponse,
                                            'Pagination' => $aPagination,
                                            'Files' => $aResponse,
                                            'Categories' => $aResultCategories
                                        )
                                    )
                                )
                            );
                            $this->setCache($sCacheIdentifier, json_encode($aApiResponse, TRUE), $aSettings);
                            $this->view->assign('value', $aApiResponse);
                            return $this->view->render();
                        }
                        break;
                    default:    /*
                         * Error:
                         * Not allowed
                         */
                        
                        $sCacheIdentifier = '';
                        $this->response->setStatus(400, 'GET \'' . $aRequestArguments['get'] . '\' is not allowed');
                        $this->view->assign('value', $aApiResponse);
                        return $this->view->render();
                }
                break;
            default:    /*
                 * Error:
                 * Not allowed
                 */
                
                $sCacheIdentifier = '';
                $this->response->setStatus(400, 'Version \'' . $aRequestArguments['version'] . '\' does not exist');
                $this->view->assign('value', $aApiResponse);
                return $this->view->render();
        }
        /*
         * Caching
         */
        
        return $sContent;
    }

    protected function renderApiResponse ($aApiResponse = []) {
        $this->view->assign('value', $aApiResponse);
        return $this->view->render();
    }
    
//    /**
//     * @return array
//     */
//    protected function getSettingsArray()
//    {
//        return $this->settings;
//    }
    
    /**
     * @param array $aSettings
     * @param array $aRequestArgumentsSanitized
     * @param int $iCountResponse
     * @param int $iLimit
     * @return array
     */
    private function getPaginationArray($aSettings = array(), $aRequestArgumentsSanitized = array(), $iCountResponse = 9999, $iLimit = 9999)
    {
        $bShowFirst = $aSettings['production']['api']['query']['files']['pagination']['showFirst'];
        $bShowPrevious = $aSettings['production']['api']['query']['files']['pagination']['showPrevious'];
        $bShowNext = $aSettings['production']['api']['query']['files']['pagination']['showNext'];
        $bShowLast = $aSettings['production']['api']['query']['files']['pagination']['showLast'];
        $iInsertBelow = $aSettings['production']['api']['query']['files']['pagination']['insertBelow'];
        $iInsertAbove = $aSettings['production']['api']['query']['files']['pagination']['insertAbove'];
        $aPagination = array();
        $iMaxDivision = $iCountResponse / $iLimit; //var_dump($iMaxDivision);
        $iMaxRound = round($iCountResponse / $iLimit); //var_dump($iMaxRound);
        $iMax = ($iMaxDivision > $iMaxRound ? ($iMaxRound + 1) : $iMaxRound); //var_dump($iMax);
        if ($iMax > 1) {
            if (array_key_exists('page', $aRequestArgumentsSanitized)) {
                if ($bShowPrevious) {
                    if ($aRequestArgumentsSanitized['page'] != 1) {
                        $aPagination[] = array(strval($aRequestArgumentsSanitized['page'] - 0 - 1), 'prev');
                    }
                }
                if ($bShowFirst) {
                    if ($aRequestArgumentsSanitized['page'] != 1 && $aRequestArgumentsSanitized['page'] - $iInsertBelow > 1) {
                        $aPagination[] = array('1', 'first');
                    }
                }
                /*
                 * get pagination below current page
                 */
                
                if ($aRequestArgumentsSanitized['page'] - $iInsertBelow > 1) {
                    $aPagination[] = array('...', 'disabled');
                }
                for ($i = $iInsertBelow; $i > 0; $i--) {
                    $j = $aRequestArgumentsSanitized['page'] - $i;
                    if ($j > 0 && $j < $aRequestArgumentsSanitized['page']) {
                        $aPagination[] = array(strval($j), '');
                    }
                }
                /*
                 * get pagination for current page
                 */
                
                $aPagination[] = array(strval($aRequestArgumentsSanitized['page']), 'active');
                /*
                 * get pagination above current page
                 */
                
                for ($i = 1; $i <= $iInsertAbove; $i++) {
                    $j = $aRequestArgumentsSanitized['page'] + $i;
                    if ($j <= $iMax) {
                        $aPagination[] = array(strval($j), '');
                    }
                }
                if ($aRequestArgumentsSanitized['page'] + $iInsertAbove <= $iMax - 1) {
                    $aPagination[] = array('...', 'disabled');
                }
            } else {
                /*
                 * get pagination for first page
                 */
                
                $aPagination[] = array('1', 'active');
                for ($i = 1; $i <= $iInsertAbove; $i++) {
                    $j = 1 + $i;
                    if ($j <= $iMax) {
                        $aPagination[] = array(strval($j), '');
                    }
                    if ($i == $iInsertAbove) {
                        if ($j <= $iMax - 1) {
                            $aPagination[] = array('...', 'disabled');
                        }
                    }
                }
            }
            if ($bShowLast) {
                if (!array_key_exists('page', $aRequestArgumentsSanitized) && $aRequestArgumentsSanitized['page'] + $iInsertAbove <= $iMax - 1) {
                    $aPagination[] = array(strval($iMax), 'last');
                } else {
                    if ($aRequestArgumentsSanitized['page'] != $iMax && $aRequestArgumentsSanitized['page'] + $iInsertAbove <= $iMax - 1) {
                        $aPagination[] = array(strval($iMax), 'last');
                    }
                }
            }
            if ($bShowNext) {
                if (!array_key_exists('page', $aRequestArgumentsSanitized)) {
                    $aPagination[] = array(strval(2), 'next');
                } else {
                    if ($aRequestArgumentsSanitized['page'] != $iMax) {
                        $aPagination[] = array(strval($aRequestArgumentsSanitized['page'] + 1), 'next');
                    }
                }
            }

        }
        return $aPagination;
    }

}
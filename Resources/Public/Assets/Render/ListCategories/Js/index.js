/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017
 * Andreas Hafner,
 * Dominik Hilser,
 * Georg Kathan (Author),
 * Hendrik Krüger,
 * Josymar Escalona Rodriguez,
 * Timo Bittner
 * for teufels GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/

// var oTxTeufelsextdlcrenderlistcategories__variables = {};
//
// var oTxTeufelsExtDlc__listcategories = function () {
//
//     var init = function () {
//         var sInput = 'sys_category_record_mm';
//         if (sInput in oTxTeufelsextdlcrenderlistcategories__variables['input']['select']) {
//             var oTxTeufelsextdlcrenderlistcategories__SelectCategories =
//                 document.getElementById(oTxTeufelsextdlcrenderlistcategories__variables['input']['select'][sInput]['id']);
//             if (oTxTeufelsextdlcrenderlistcategories__SelectCategories != null) {
//                 /**
//                  * onload
//                  */
//                 oTxTeufelsextdlcrenderlistcategories__variables['input']['select'][sInput]['value'] =
//                     oTxTeufelsextdlcrenderlistcategories__SelectCategories.options[oTxTeufelsextdlcrenderlistcategories__SelectCategories.selectedIndex].value;
//                 /**
//                  * onchange
//                  */
//                 oTxTeufelsextdlcrenderlistcategories__SelectCategories.onchange = function () {
//                     oTxTeufelsextdlcrenderlistcategories__variables['input']['select'][sInput]['value'] = this.options[this.selectedIndex].value;
//                     console.log('oncange');
//                     setHiddenFieldValues();
//                 };
//             }
//         }
//
//         return true;
//     };
//
//     var setHiddenFieldValues = function () {
//         var sInput = 'sys_category_record_mm';
//         if (sInput in oTxTeufelsextdlcrenderlistcategories__variables['input']['hidden']) {
//             var aValue = new Array();
//
//             if (sInput in oTxTeufelsextdlcrenderlistcategories__variables['input']['select']) {
//                 if (oTxTeufelsextdlcrenderlistcategories__variables['input']['select'][sInput]['value'] != '') {
//                     aValue.push(oTxTeufelsextdlcrenderlistcategories__variables['input']['select'][sInput]['value']);
//                 }
//             }
//             var sValue = aValue.length > 0 ? aValue.join(",") : '';
//             oTxTeufelsextdlcrenderlistcategories__variables['input']['hidden'][sInput]['value'] = sValue;
//
//             var aTxTeufelsextdlcTeufelsextdlcrenderfilter__hiddenPages =
//                 document.getElementsByClassName(oTxTeufelsextdlcrenderlistcategories__variables['input']['hidden'][sInput]['class']);
//
//             if (aTxTeufelsextdlcTeufelsextdlcrenderfilter__hiddenPages.length > 0) {
//                 for (var h = 0; h < aTxTeufelsextdlcTeufelsextdlcrenderfilter__hiddenPages.length; h++) {
//                     aTxTeufelsextdlcTeufelsextdlcrenderfilter__hiddenPages[h].value = oTxTeufelsextdlcrenderlistcategories__variables['input']['hidden'][sInput]['value'];
//                 }
//             }
//
//
//             // if (aTxTeufelsextdlcTeufelsextdlcrenderfilter__hiddenPages != null) {
//             //     aTxTeufelsextdlcTeufelsextdlcrenderfilter__hiddenPages.value =
//             //         oTxTeufelsextdlcTeufelsextdlcrenderfilter__variables['paramsFilter']['hidden'][sSelectField]['value'];
//             //
//             //
//             // }
//
//         }
//
//     };
//
//     if (init()) {
//         setHiddenFieldValues();
//     }
//
// };
//
// /*
//  * Wait till window load.
//  *
//  */
// var iTxTeufelsextdlcTeufelsextdlcrenderlistcategories__interval = setInterval(function() {
//
//     if (typeof teufels_cfg_typoscript__windowLoad == "boolean" && teufels_cfg_typoscript__windowLoad) {
//
//         clearInterval(iTxTeufelsextdlcTeufelsextdlcrenderlistcategories__interval);
//
//         if (typeof teufels_cfg_typoscript_sStage != 'undefined' &&
//             (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development")) {
//             console.info('tx_teufels_ext_dlc_render_categories :: index.js :: loaded');
//         }
//
//         /*
//          * Check if filter is present in html
//          */
//         var oTxTeufelsextdlcTeufelsextdlcrenderfilter__filter = document.getElementsByClassName('tx-teufels-ext-dlc__listcategories');
//
//         if (oTxTeufelsextdlcTeufelsextdlcrenderfilter__filter.length > 0) {
//
//             if (typeof teufels_cfg_typoscript_sStage != 'undefined' &&
//                 (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development")) {
//                 console.info('.tx-teufels-ext-tx_teufels_ext_dlc_render_categories :: present');
//             }
//
//             oTxTeufelsextdlcrenderlistcategories__variables = {
//                 'input': {
//                     'hidden': {
//                         'sys_category_record_mm': {
//                             'class': 'hidden sys_category_record_mm',
//                             'param': 'tx_teufelsextdlc_teufelsextdlcrenderlist[sys_file_metadata_pages_mm][mm]',
//                             'value': ''
//                         }
//                     },
//                     'select': {
//                         'sys_category_record_mm': {
//                             'id': 'sys_category_record_mm',
//                             'value': ''
//                         }
//                     }
//                 }
//             };
//
//             oTxTeufelsExtDlc__listcategories();
//
//         }
//     }
// });

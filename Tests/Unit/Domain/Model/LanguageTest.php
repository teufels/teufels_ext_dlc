<?php

namespace TEUFELS\TeufelsExtDlc\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \TEUFELS\TeufelsExtDlc\Domain\Model\Language.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class LanguageTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \TEUFELS\TeufelsExtDlc\Domain\Model\Language
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \TEUFELS\TeufelsExtDlc\Domain\Model\Language();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getIso6391ReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getIso6391()
		);
	}

	/**
	 * @test
	 */
	public function setIso6391ForStringSetsIso6391()
	{
		$this->subject->setIso6391('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'iso6391',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getIso31661ReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getIso31661()
		);
	}

	/**
	 * @test
	 */
	public function setIso31661ForStringSetsIso31661()
	{
		$this->subject->setIso31661('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'iso31661',
			$this->subject
		);
	}
}
